$(document).ready(function(){

    
    //search the details of user and display them as option
    $('.user_search').select2({
        placeholder: 'Select user',
        minimumInputLength: 3 ,
        ajax: {
          url: root_url+'user-ajax',
          dataType: 'json',
          delay:250,
           
          processResults: function (data) {

            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name+" | "+item.email+" | "+item.phone_number,
                        id: item.id
                    }

                })
            };
          
          },
          cache: true
        } 

      });


    //search the details of properties and display them as option
    $('.property_search').select2({
        placeholder: 'Select Property',
        minimumInputLength: 3 ,
        ajax: {
          url: root_url+'property-ajax',
          dataType: 'json',
          delay:250,
           
          processResults: function (data) {

            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }

                })
            };
          
          },
          cache: true
        } 

      });
     //search the details of location and display them as option
     $('.location_search').select2({
        placeholder: 'Select Location',
        minimumInputLength: 3 ,
        ajax: {
          url: root_url+'location-ajax',
          dataType: 'json',
          delay:250,
           
          processResults: function (data) {

            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }

                })
            };
          
          },
          cache: true
        }


      });

     //search the details of categories and display them as option
     $('.category_search').select2({
        placeholder: 'Select Category',
        minimumInputLength: 3 ,
        ajax: {
          url: root_url+'category-ajax',
          dataType: 'json',
          delay:250,
           
          processResults: function (data) {

            return {
              results:  $.map(data, function (item) {
                    return {
                        text: item.name,
                        id: item.id
                    }

                })
            };
          
          },
          cache: true
        }


      });


}); //document.ready end