$(document).ready(function(){

    $('.glyphicon-trash').click(function(){
        var r = confirm("Are you sure?");
        if (r == true) {
          $(this).parent().parent('form').submit();
        }    
    });

   //check permissions in roles create/edit
    $check= 0;
    $(document).on('click','.check_all', function() { 
        if($check== 0){
          $(this).parent().find('input:checkbox').prop('checked',true);
          $(this).text('Uncheck all');
          $check++;
        }
        else{
          $(this).parent().find('input:checkbox').removeAttr('checked');
          $(this).text('Check all');
          $check--;
        } 
    });

    

}); //document.ready end