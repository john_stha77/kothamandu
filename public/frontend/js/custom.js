$(document).ready(function(){

	//show related hidden inputs
	//when be and agent option is "Yes"
	$(".make_agent").change(function(){ 
        $value= $(this).val(); 
        if($value=="1"){
        	$(".expertise_location").removeClass('hide');
        }
        else{
        	$(".expertise_location").addClass('hide');
        }
    });

    //for owl caraousal
    $('.owl-one').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });

});