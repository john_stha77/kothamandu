<?php
namespace Modules\Featured;
use Core\Repository\CoreRepository;
use Modules\Featured\FeaturedModel;
use Modules\Featured\FeaturedViewModel;
class FeaturedRepository extends CoreRepository{
    public function __construct(FeaturedModel $featuredModel){
        parent::__construct($featuredModel);
    }
}