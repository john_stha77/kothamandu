<?php

namespace Modules\Featured;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Featured\FeaturedRepository; 
use Modules\Featured\FeaturedRequest;

class FeaturedController extends Controller
{
    private $featuredRepo;
    public function __construct(FeaturedRepository $featuredRepo){
        $this->featuredRepo =  $featuredRepo;

    } 
     
    public function index()
    {
        $posts = $this->featuredRepo->getPaginate(10,['property']) ; 
         
        return view('Featured.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Featured.views.create');
    }
    public function store(FeaturedRequest $request)
    {
        $featured = $this->featuredRepo->store($request->all());

        return redirect()->route('featured.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->featuredRepo->getById($id);
        
        return view('Featured.views.edit', compact('posts'));
    }
    public function update(FeaturedRequest $request, $id)
    {
        $featured= $this->featuredRepo->update($request->all(),$id);

        return redirect()->route('featured.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->featuredRepo->destroy($id); 
        return redirect()->route('featured.index')->withFlashSuccess('It is deleted successfully.');
    } 
}