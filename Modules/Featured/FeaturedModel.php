<?php

namespace Modules\Featured;
use Core\Model\CoreModel; 

class FeaturedModel extends CoreModel 
{	
	public $table = "featured";
    protected $guarded = ['id'];

    public function property(){
    	return $this->belongsTo('Modules\Property\PropertyModel', 'property_id'); 
    }
    public function property_images(){
    	return $this->hasMany('Modules\Property\PropertyImageModel', 'property_id'); 
    }
}