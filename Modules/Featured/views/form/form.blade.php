<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Active</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('active', ['1'=>'Yes','0'=>'No'], null, ['placeholder'=>'--Select--','class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Order
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::number('order', null ,['class' => 'form-control','autocomplete'=>'off'] ) !!}
                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 user_search_label" for="first-name">
                        Property
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12 user_id">
                        <select class="property_search form-control" name="property_id">
                            @if(isset($posts->property_id))
                            <option value="{{$posts->property_id}}">{{$posts->property->name}}</option>
                            @endif
                        </select>
                        </div>
                    </div>        
                     
                    
                     