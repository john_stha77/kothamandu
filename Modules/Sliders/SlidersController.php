<?php

namespace Modules\Sliders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Sliders\SlidersRepository; 
use Modules\Sliders\SlidersRequest;
use Modules\Misc\FileUpload\FileUploadController;
use Carbon\Carbon;

class SlidersController extends Controller
{
    private $slidersRepo;
    public function __construct(SlidersRepository $slidersRepo){
        $this->slidersRepo =  $slidersRepo;
        $this->filepath= "/files/sliders/";
    } 
     
    public function index()
    {
        $posts = $this->slidersRepo->getPaginate() ; 
        $filepath= $this->filepath;
        return view('Sliders.views.index', compact('posts','filepath'));
    }
    public function create()
    { 
        return view('Sliders.views.create');
    }
    public function store(SlidersRequest $request)
    {
        $file= $request->file('icon');
        $filepath= $this->filepath;
        
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=true, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type']; 
        } 
        $sliders = $this->slidersRepo->store($request->all());

        return redirect()->route('sliders.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->slidersRepo->getById($id);
        
        return view('Sliders.views.edit', compact('posts'));
    }
    public function update(SlidersRequest $request, $id)
    {
        $slider = $this->slidersRepo->getById($id);
        $filepath= $this->filepath;
        if($request->hasFile('icon'))
        {   
            //delete previous icon 
            $file_deleted= fileDeleteIfExist($filepath,$slider->image_file_name);
        }   
        $file= $request->file('icon'); 
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=false, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type'];
            $request['image_updated_at']= Carbon::now(getCurrentTimeZone()); 
        } 
        $sliders= $this->slidersRepo->update($request->all(),$id);

        return redirect()->route('sliders.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->slidersRepo->destroy($id); 
        return redirect()->route('sliders.index')->withFlashSuccess('It is deleted successfully.');
    } 
}