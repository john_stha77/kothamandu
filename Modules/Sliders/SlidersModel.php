<?php

namespace Modules\Sliders;
use Core\Model\CoreModel; 

class SlidersModel extends CoreModel 
{	
	public $table = "sliders";
    protected $guarded = ['id','icon'];
}