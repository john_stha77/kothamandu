<?php
namespace Modules\Sliders;
use Core\Repository\CoreRepository;
use Modules\Sliders\SlidersModel;
use Modules\Sliders\SlidersViewModel;
class SlidersRepository extends CoreRepository{
    public function __construct(SlidersModel $slidersModel){
    	$this->slidersModel= $slidersModel;
        parent::__construct($slidersModel);
    }

    public function getAllOrderBy($orderBy,$with=null){
        $result=null;
        foreach ($orderBy as $key => $value) { 
                $result= ($with==null) ? $this->slidersModel->where("active",true)->orderBy($key,$value)->get() : $this->slidersModel->with($with)->where("active",true)->orderBy($key,$value)->get(); 
            break;
        }
        
        return $result; 
    }
}