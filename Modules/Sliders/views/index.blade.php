@extends('layouts.backend.master')

@section('content')

<div class="x_panel">

        <div class="x_title">
            <h2>Sliders</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <span><a href="{!! route('sliders.create') !!}" class="btn btn-primary">Add New  Sliders</a>
                    </span> 
                </li>
            </ul>  
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>S.N.</th> 
                    <th>Image</th>
                    <th>Description</th>
                    <th>Active</th>
                    <th>Order</th> 
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    <?php  $i=1; ?>
                @foreach($posts as $p) 
                <tr>
                    <td> {{ $i }}</td> 
                    <td> <img src="{!! asset($filepath.$p->image_file_name) !!}"  height="60" width="60">  </td>
                    <td> {!! $p->description !!}</td>
                    <td> {{($p->active== 1) ? "Yes" : "No" }}</td>
                    <td> {{ $p->order}}</td> 
                    <td> 
                    <a href=" {{ route('sliders.edit',$p->id) }}" class="action-btns">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    {{Form::open(['route'=>['sliders.destroy', $p->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                         <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                    {{Form::close()}}
                   
                    </td>
                </tr>
                <?php $i++; ?> 
                @endforeach
                
                </tbody>
            </table>
            
            {{ $posts->links() }}
        </div>
    </div>


@endsection