<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Name
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Link
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('link', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Description
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::textarea('description', null ,['class' => 'form-control', 'id' => 'summary-ckeditor'] ) !!}
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Active  </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('active',  ['1'=>'Yes',''=>'No'], null, ['placeholder'=>'--Select--','class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Image </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            @if(isset($posts))
                            @if($posts->image_file_name != null)
                                 <img class="img-thumbnail"  src="{{asset('files/sliders/small/'.$posts->image_file_name)}}"  width="200">
                            @endif
                            @endif
                        <div class="browse input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    <i class="fa fa-folder-open"></i>
                                    Browse {{Form::file('icon',null, ['class'=>'form-control'])}}
                                </span>
                            </label>
                            <input type="text" class="form-control" disabled>
                        </div>
                        <!-- {{Form::file('document',null, ['class'=>'form-control'])}} -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Order
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::number('order', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>