<?php

namespace Modules\Roles;
use Core\Model\CoreModel; 

class RolesModel extends CoreModel 
{	
	public $table = "roles";
    protected $guarded = ['id','permission_id'];
}