<?php

namespace Modules\Roles;
use Illuminate\Database\Eloquent\Model;

class RolesPermissionsModel extends Model 
{	
	public $table = "permissions_roles";
    protected $guarded = ['id'];
}