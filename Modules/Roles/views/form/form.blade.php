<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Name
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Identifier
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('identifier', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Description
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::textarea('description', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Permissions:
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12 permissions"> 
                            <a href="javascript:void(0);" class="btn btn-success check_all">Check all</a><br><br> 

                                @foreach($all_permissions as $permission)
                                    @if(in_array($permission->id, $permission_ids))
                                        <input type="checkbox" name="permission_id[]" value="{{$permission->id}}" checked>&nbsp {{$permission->name}}<br>
                                    @else
                                        <input type="checkbox" name="permission_id[]" value="{{$permission->id}}">&nbsp {{$permission->name}}<br>
                                    @endif
                                @endforeach 
                        
                        </div>
                    </div>