<?php

namespace Modules\Roles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Roles\RolesRepository; 
use Modules\Roles\RolesPermissionsRepository; 
use Modules\Permissions\PermissionsRepository;
use Modules\Roles\RolesRequest;

class RolesController extends Controller
{
    private $rolesRepo;
    public function __construct(RolesRepository $rolesRepo,PermissionsRepository $permissionsRepo,RolesPermissionsRepository $rolesPermissionsRepo){
        $this->rolesRepo =  $rolesRepo;
        $this->permissionsRepo =  $permissionsRepo;
        $this->rolesPermissionsRepo= $rolesPermissionsRepo;

    } 
     
    public function index()
    {
        $posts = $this->rolesRepo->getPaginate() ; 
         
        return view('Roles.views.index', compact('posts'));
    }
    public function create()
    { 
        $permission_ids=array();
        $all_permissions = $this->permissionsRepo->getAll();
        return view('Roles.views.create',compact('all_permissions','permission_ids'));
    }
    public function store(RolesRequest $request)
    {
        $role = $this->rolesRepo->store($request->all()); 
        if($role){  
            foreach ($request['permission_id'] as $p) {
                $data['role_id']= $role->id;
                $data['permission_id']= intval($p);
                $this->rolesPermissionsRepo->store($data);
            }  
        }

        return redirect()->route('roles.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->rolesRepo->getById($id);
        $given_permissions= $this->rolesPermissionsRepo->getAllByRoleId($id); 
        $permission_ids=array();
        if($given_permissions!=null){
            foreach ($given_permissions as $p) {
                $permission_ids[]=  $p->permission_id;
            }
        }
        

        
        $all_permissions = $this->permissionsRepo->getAll();
        
        return view('Roles.views.edit', compact('posts','permission_ids','all_permissions'));
    }
    public function update(RolesRequest $request, $id)
    {
        $this->validate($request,[
            'identifier'=>'required|unique:roles,identifier,'.$id, 
        ]);
        $roles= $this->rolesRepo->update($request->all(),$id);
        if($roles){ 
            //delete all existing roles permissions
            $this->rolesPermissionsRepo->destroyByRoleId($id); 
            //create new
            foreach ($request['permission_id'] as $permission_id) {
                $data['role_id']= $id;
                $data['permission_id']= intval($permission_id); 

                $this->rolesPermissionsRepo->store($data);
            } 
        }

        return redirect()->route('roles.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->rolesRepo->destroy($id); 
        return redirect()->route('roles.index')->withFlashSuccess('It is deleted successfully.');
    } 
}