<?php
namespace Modules\Roles;
use Core\Repository\CoreRepository;
use Modules\Roles\RolesModel;
use Modules\Roles\RolesViewModel;
class RolesRepository extends CoreRepository{
    public function __construct(RolesModel $rolesModel){
    	$this->rolesModel= $rolesModel;
        parent::__construct($rolesModel);
    }
    public function pluck(){
    	return $this->rolesModel->pluck('name','id');
    }
}