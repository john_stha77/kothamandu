<?php
namespace Modules\Roles;
use Core\Repository\CoreRepository;
use Modules\Roles\RolesPermissionsModel; 
class RolesPermissionsRepository extends CoreRepository{
    public function __construct(RolesPermissionsModel $rolesPermissionsModel){
        $this->rolesPermissionsModel= $rolesPermissionsModel;
        parent::__construct($rolesPermissionsModel);
    }
    public function getAllByRoleId($role_id){
        return  $this->rolesPermissionsModel->where('role_id',$role_id)->get();
    }
    public function destroyByRoleId($role_id){
    	return  $this->rolesPermissionsModel->where('role_id',$role_id)->delete();
    }
}