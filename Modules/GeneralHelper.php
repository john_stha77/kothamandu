<?php
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

//file delete if it exists
function fileDeleteIfExist($directory,$file_name){  
	$public_disk= Storage::disk('public');
    $directories = $public_disk->directories($directory);  
    //delete if main file if exixts
    $bigFile= $directory.$file_name;  
    if(is_file(public_path().$bigFile)){  
    	$public_disk->delete($bigFile);  
    }
    //now get all directories and delete files in them 
    // dd($directories);
    foreach ($directories as $d) { 
        $file= $d."/".$file_name;  
        if(is_file(public_path()."/".$file)){ 
           $check=$public_disk->delete($file); 
        }
    }
    return true;
}
function getCurrentTimeZone(){
    $timezone=  "Asia/Kathmandu"; 
    return ($timezone);
}
function getCurrentTime(){
    $now=  Carbon::now("Asia/Kathmandu"); 
    return ($now);
}



//for frontend