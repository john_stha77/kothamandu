<?php
namespace Modules\Advs;
use Core\Repository\CoreRepository;
use Modules\Advs\AdvsModel;
use Modules\Advs\AdvsViewModel;
class AdvsRepository extends CoreRepository{
    public function __construct(AdvsModel $advsModel){
        parent::__construct($advsModel);
    }
}