<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Link
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('link', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Image </label>
    <div class="col-md-6 col-sm-7 col-xs-12"> 
        @if(isset($posts))
        @if($posts->image_file_name !=null)
            <img class="img-thumbnail" src="{{asset('files/category/small/'.$posts->image_file_name)}}"  height="150" width="150">
        @endif
        @endif
        Browse {{Form::file('icon',null, ['class'=>'form-control'])}} 
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Active
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {{Form::select('active',  [ 1 =>'Yes',0 =>'No'], null, ['class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Order
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::number('order', null ,['class' => 'form-control'] ) !!}
    </div>
</div>