<?php

namespace Modules\Advs;
use Core\Model\CoreModel; 

class AdvsModel extends CoreModel 
{	
	public $table = "advs";
    protected $guarded = ['id','icon'];
}