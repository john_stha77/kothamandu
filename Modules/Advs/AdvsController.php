<?php

namespace Modules\Advs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Advs\AdvsRepository; 
use Modules\Advs\AdvsRequest;
use Modules\Misc\FileUpload\FileUploadController;

class AdvsController extends Controller
{
    private $advsRepo;
    public function __construct(AdvsRepository $advsRepo){
        $this->advsRepo =  $advsRepo;
        $this->filepath = "/files/advs/";
    } 
     
    public function index()
    {
        $posts = $this->advsRepo->getPaginate() ; 
         
        return view('Advs.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Advs.views.create');
    }
    public function store(AdvsRequest $request)
    {
        $file= $request->file('icon');
        $filepath= $this->filepath;
        
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=true, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type'];
             
        }
        $advs = $this->advsRepo->store($request->all());

        return redirect()->route('advs.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->advsRepo->getById($id);
        
        return view('Advs.views.edit', compact('posts'));
    }
    public function update(AdvsRequest $request, $id)
    {   
        $filepath= $this->filepath;
        $details =  $this->advsRepo->getById($id);
        if($request->hasFile('icon'))
        {   
            //delete previous icon 
            $file_deleted= fileDeleteIfExist($filepath,$details->logo_file_name);
        }
        $file= $request->file('icon');
        
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=true, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type'];
            $request['image_updated_at']= getCurrentTime();
        }

        $advs= $this->advsRepo->update($request->all(),$id);

        return redirect()->route('advs.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->advsRepo->destroy($id); 
        return redirect()->route('advs.index')->withFlashSuccess('It is deleted successfully.');
    } 
}