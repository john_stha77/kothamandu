<?php

namespace Modules\Agents;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Agents\AgentsRepository; 
use Modules\Agents\AgentsRequest;

class AgentsController extends Controller
{
    private $agentsRepo;
    public function __construct(AgentsRepository $agentsRepo){
        $this->agentsRepo =  $agentsRepo;

    } 
     
    public function index()
    {
        $posts = $this->agentsRepo->getPaginate() ; 
         
        return view('Agents.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Agents.views.create');
    }
    public function store(AgentsRequest $request)
    {
        $agents = $this->agentsRepo->store($request->all());

        return redirect()->route('agents.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->agentsRepo->getById($id);
        
        return view('Agents.views.edit', compact('posts'));
    }
    public function update(AgentsRequest $request, $id)
    {
        $agents= $this->agentsRepo->update($request->all(),$id);

        return redirect()->route('agents.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->agentsRepo->destroy($id); 
        return redirect()->route('agents.index')->withFlashSuccess('It is deleted successfully.');
    } 
}