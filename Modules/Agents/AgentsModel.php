<?php

namespace Modules\Agents;
use Core\Model\CoreModel; 

class AgentsModel extends CoreModel 
{	
	public $table = "agents";
    protected $guarded = ['id'];

    public function location(){
    	return $this->belongsTo('Modules\Locations\LocationsModel', 'expertise_location_id'); 
    }
    public function user(){
    	return $this->belongsTo('Modules\Users\UsersModel', 'user_id'); 
    }
}