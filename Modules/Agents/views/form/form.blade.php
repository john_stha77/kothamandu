<div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 user_search_label" for="first-name">
                User
                </label>
                <div class="col-md-6 col-sm-7 col-xs-12 user_id">
                <select class="user_search form-control" name="user_id">
                    @if(isset($posts->user_id))
                    <option value="{{$posts->user_id}}">{{$posts->user->name}}</option>
                    @endif
                </select>
                </div>
            </div> 
<div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12 user_search_label" for="first-name">
                Expertise location
                </label>
                <div class="col-md-6 col-sm-7 col-xs-12 user_id">
                <select class="location_search form-control" name="expertise_location_id">
                    @if(isset($posts->expertise_location_id))
                    <option value="{{$posts->expertise_location_id}}">{{$posts->location->name}}</option>
                    @endif
                </select>
                </div>
            </div> 