<?php
namespace Modules\Agents;
use Core\Repository\CoreRepository;
use Modules\Agents\AgentsModel;
use Modules\Agents\AgentsViewModel;
class AgentsRepository extends CoreRepository{
    public function __construct(AgentsModel $agentsModel){
    	$this->agentsModel= $agentsModel;
        parent::__construct($agentsModel);
    }

    public function getOneByUserId($user_id){
        return  $this->agentsModel->where('user_id',$user_id)->first();
    }
    public function updateByUserId($inputData,$user_id){
		return $this->agentsModel->where("user_id",$user_id)->update($inputData);
	}
	public function destroyByUserId($user_id){
		$agent= $this->getOneByUserId($user_id);
		$this->agentsModel->destroy($agent->id);
		return true;
	}
    
    public function checkIfExists($user_id){
        $exists=false;
        if($this->getOneByUserId($user_id)){
            $exists=true;
        }
        return $exists;
    }

    
}