<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Name
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Similar Names (Use pipe seperator |)
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::textarea('similar_names', null ,['class' => 'form-control', 'placeholder' => 'Baneshwor | Old Baneshwor | Mid Baneshwor'] ) !!}
                        </div>
                    </div>
                     
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Search Address
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('map_name', null ,['class' => 'form-control', 'id' => 'searchInput', 'placeholder' => 'Enter a location'] ) !!}
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Lat
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('lat', null ,['class' => 'form-control', 'id' => 'lat'] ) !!}
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Lng
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('lng', null ,['class' => 'form-control', 'id' => 'lng'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Map
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                            <div class="map" id="map" style="width: 500px; height: 450px;"></div>
                        </div>
                    </div><br>