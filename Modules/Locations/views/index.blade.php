@extends('layouts.backend.master')

@section('content')

<div class="x_panel">

        <div class="x_title">
            <h2>Locations</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <span><a href="{!! route('locations.create') !!}" class="btn btn-primary">Add New  Locations</a>
                    </span> 
                </li>
            </ul>  
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>S.N.</th> 
                    <th>Location</th>
                    <th>Similar Name</th>              
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    <?php  $i=1; ?>
                @foreach($posts as $p) 
                <tr>
                    <td> {{ $i }}</td> 
                    <td> {{ $p->name}}</td>
                    <td> {{ $p->similar_names}}</td> 
                    <td> {{ $p->lat}}</td>
                    <td> {{ $p->lng}}</td>
                    <td> 
                    <a href=" {{ route('locations.edit',$p->id) }}" class="action-btns">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    {{Form::open(['route'=>['locations.destroy', $p->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                        <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>   
                    {{Form::close()}}
                   
                    </td>
                </tr>
                <?php $i++; ?> 
                @endforeach
                
                </tbody>
            </table>
            
            {{ $posts->links() }}
        </div>
    </div>


@endsection