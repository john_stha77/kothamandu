<?php
namespace Modules\Locations;
use Core\Repository\CoreRepository;
use Modules\Locations\LocationsModel;
use Modules\Locations\LocationsViewModel;
class LocationsRepository extends CoreRepository{
    public function __construct(LocationsModel $locationsModel){
        parent::__construct($locationsModel);
    }
}