<?php

namespace Modules\Locations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Locations\LocationsRepository; 
use Modules\Locations\LocationsRequest;

class LocationsController extends Controller
{
    private $locationsRepo;
    public function __construct(LocationsRepository $locationsRepo){
        $this->locationsRepo =  $locationsRepo;

    } 
     
    public function index()
    {
        $posts = $this->locationsRepo->getPaginate() ; 
         
        return view('Locations.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Locations.views.create');
    }
    public function store(LocationsRequest $request)
    {
        $lat= $request['lat'];
        $lng= $request['lng'];

        // $matrix = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=27.69692,85.34394529999997&destinations='.$lat.','.$lng.'&key=AIzaSyAr0ymK2QY0U9dRpjQFurcChfZDzkWW79k'), true); 
        
        // $distance= explode(" ", $matrix['rows'][0]['elements'][0]['distance']['text']);
        // $duration= explode(" ", $matrix['rows'][0]['elements'][0]['duration']['text']);  
        // //convert mile to meter
        // $request['distance'] = intval (1609.34 * $distance[0]);
        // $request['duration'] = intval($duration[0]);

        $locations = $this->locationsRepo->store($request->all());

        return redirect()->route('locations.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->locationsRepo->getById($id);
        
        return view('Locations.views.edit', compact('posts'));
    }
    public function update(LocationsRequest $request, $id)
    {
        $lat= $request['lat'];
        $lng= $request['lng'];

        // $matrix = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=27.69692,85.34394529999997&destinations='.$lat.','.$lng.'&key=AIzaSyAr0ymK2QY0U9dRpjQFurcChfZDzkWW79k'), true);
         
        // $distance= explode(" ", $matrix['rows'][0]['elements'][0]['distance']['text']);
        // $duration= explode(" ", $matrix['rows'][0]['elements'][0]['duration']['text']);  
        // //convert mile to meter
        // $request['distance'] = intval(1609.34 * $distance[0]);
        // $request['duration'] = intval($duration[0]);
        
        $locations= $this->locationsRepo->update($request->all(),$id);

        return redirect()->route('locations.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->locationsRepo->destroy($id); 
        return redirect()->route('locations.index')->withFlashSuccess('It is deleted successfully.');
    } 
}