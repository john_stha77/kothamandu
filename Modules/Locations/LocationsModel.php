<?php

namespace Modules\Locations;
use Core\Model\CoreModel; 

class LocationsModel extends CoreModel 
{	
	public $table = "locations";
    protected $guarded = ['id'];
}