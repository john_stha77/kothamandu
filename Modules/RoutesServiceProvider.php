<?php
namespace Modules;

class RoutesServiceProvider {
    public function __construct(){
    }
    public  function runRoutes(){
		require_once(base_path('Modules/Enquiries/routes.php')); 
		require_once(base_path('Modules/Sliders/routes.php'));
		require_once(base_path('Modules/Newsletter/routes.php'));
		require_once(base_path('Modules/ApplyProperty/routes.php'));
		require_once(base_path('Modules/Agents/routes.php'));
		require_once(base_path('Modules/Permissions/routes.php'));
		require_once(base_path('Modules/Roles/routes.php'));
		require_once(base_path('Modules/Users/routes.php'));
		require_once(base_path('Modules/Locations/routes.php'));
		require_once(base_path('Modules/Category/routes.php')); 
		require_once(base_path('Modules/Generals/routes.php'));
		require_once(base_path('Modules/Featured/routes.php'));
		require_once(base_path('Modules/Property/routes.php'));
		require_once(base_path('Modules/Advs/routes.php')); 
        require_once(base_path('Modules/Admin/routes.php'));  

    }

}
