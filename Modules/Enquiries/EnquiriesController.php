<?php

namespace Modules\Enquiries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Enquiries\EnquiriesRepository; 
use Modules\Enquiries\EnquiriesRequest;

class EnquiriesController extends Controller
{
    private $enquiriesRepo;
    public function __construct(EnquiriesRepository $enquiriesRepo){
        $this->enquiriesRepo =  $enquiriesRepo;

    } 
     
    public function index()
    {
        $posts = $this->enquiriesRepo->getPaginate() ; 
         
        return view('Enquiries.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Enquiries.views.create');
    }
    public function store(EnquiriesRequest $request)
    {
        $enquiries = $this->enquiriesRepo->store($request->all());

        return redirect()->route('enquiries.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->enquiriesRepo->getById($id);
        
        return view('Enquiries.views.edit', compact('posts'));
    }
    public function update(EnquiriesRequest $request, $id)
    {
        $enquiries= $this->enquiriesRepo->update($request->all(),$id);

        return redirect()->route('enquiries.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->enquiriesRepo->destroy($id); 
        return redirect()->route('enquiries.index')->withFlashSuccess('It is deleted successfully.');
    } 
}