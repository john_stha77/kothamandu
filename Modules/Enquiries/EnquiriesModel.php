<?php

namespace Modules\Enquiries;
use Core\Model\CoreModel; 

class EnquiriesModel extends CoreModel 
{	
	public $table = "enquiries";
    protected $guarded = ['id'];
}