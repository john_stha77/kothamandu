<?php
namespace Modules\Enquiries;
use Core\Repository\CoreRepository;
use Modules\Enquiries\EnquiriesModel;
use Modules\Enquiries\EnquiriesViewModel;
class EnquiriesRepository extends CoreRepository{
    public function __construct(EnquiriesModel $enquiriesModel){
        parent::__construct($enquiriesModel);
    }
}