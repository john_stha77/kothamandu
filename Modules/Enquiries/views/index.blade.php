@extends('layouts.backend.master')

@section('content')

<div class="x_panel">

        <div class="x_title">
            <h2>Enquiries</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <span><a href="{!! route('enquiries.create') !!}" class="btn btn-primary">Add New  Enquiries</a>
                    </span> 
                </li>
            </ul>  
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>S.N.</th> 
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone Number</th>
                    <th>Message</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    <?php  $i=1; ?>
                @foreach($posts as $p) 
                <tr>
                    <td> {{ $i }}</td> 
                    <td>{{$p->name}}</td>
                    <td>{{$p->email}}</td>
                    <td>{{$p->phone_number}}</td> 
                    <td>{{$p->message}}</td>
                    <td> 
                     
                    {{Form::open(['route'=>['enquiries.destroy', $p->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                         <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                    {{Form::close()}}
                   
                    </td>
                </tr>
                <?php $i++; ?> 
                @endforeach
                
                </tbody>
            </table>
            
            {{ $posts->links() }}
        </div>
    </div>


@endsection