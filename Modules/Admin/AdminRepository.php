<?php
namespace Modules\Admin;
use Core\Repository\CoreRepository;
use Modules\Admin\AdminModel;
use Modules\Admin\AdminViewModel;
class AdminRepository extends CoreRepository{
    public function __construct(AdminModel $adminModel){
        parent::__construct($adminModel);
    }
}