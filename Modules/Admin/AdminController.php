<?php

namespace Modules\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Admin\AdminRepository;
use Modules\Admin\AdminInfoRepository;
use Modules\Admin\AdminRequest;

class AdminController extends Controller
{
    private $adminRepo;
    public function __construct(AdminRepository $adminRepo){
        $this->adminRepo =  $adminRepo;

    } 
     
    public function index()
    {
        
        return view('Admin.views.index');
    }
}
    