<?php

namespace Modules\Admin;
use Core\Model\CoreModel; 

class AdminModel extends CoreModel 
{	
	public $table = "admin";
    protected $guarded = ['id'];
}