@extends('layouts.backend.master')

@section('content')

<div class="x_panel">

        <div class="x_title">
            <h2>Admin</h2>
             
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
             <h3>Welcome, {{ucwords(Auth::user()->name)}} !!</h3>
        </div>
    </div>


@endsection