<?php

namespace Modules\Users;
use Illuminate\Database\Eloquent\Model;

class UserSignInDetailsModel extends Model 
{	
	public $table = "user_signin_details";
    protected $guarded = ['id'];
}