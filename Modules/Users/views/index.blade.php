@extends('layouts.backend.master')

@section('content') 
<div class="x_panel">

        <div class="x_title">
            <h2>Users</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <span><a href="{!! route('users.create') !!}" class="btn btn-primary">Add New  Users</a>
                    </span> 
                </li>
            </ul>  
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
            <table id="datatable" class="table table-striped table-bordered">
                <thead> 
                    <th>S.N.</th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th width="10%">Role</th>
                    <th>Active</th>
                     
                   <th>Action</th>

                </tr>
                </thead>

                <tbody>
                    <?php  $i=1; ?>
                @foreach($posts as $p)

                <tr>
                   <td> {{ $i }}</td>
                    
                    <td>  <img class="img-thumbnail" src="{{asset('files/'.(isset($p->user_details) ? 'users/'.$p->user_details->avatar_file_name : 'generals/'.$generals['default_image_name']))}}" height="100" width="100"></td>
                    <td> {{ $p->name}}</td>
                    <td> {{ $p->email}}</td>
                    <td> {{ $p->phone_number}}</td>
                    <td> {{ (isset($p->user_details) ? $p->user_details->address : "")}}</td>
                    <td> {{ isset($p->role) ? $p->role->name : "" }}</td>
                    <td> {{ ($p->active== "1") ? "Yes" : "No" }}</td>
                    
                                        
                    <td>
                   
                    <a href=" {{ route('users.edit',$p->id) }}" class="action-btns">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    {{Form::open(['route'=>['users.destroy', $p->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                        <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                    {{Form::close()}}
                   
                    </td>
                </tr>
                 <?php $i++; ?>

                @endforeach
                </tbody>
            </table>
             {{ $posts->links() }}
        </div>
    </div>


@endsection