                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Name
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Image </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        @if(isset($posts->user_details))
                        @if($posts->user_details->avatar_file_name !=null)
                            <img class="img-thumbnail" src="{{asset('files/users/small/'.$posts->user_details->avatar_file_name)}}"  height="150" width="150">
                        @endif 
                        @endif 
                        <div class="browse input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    <i class="fa fa-folder-open"></i>
                                    Browse {{Form::file('icon',null, ['class'=>'form-control'])}}
                                </span>
                            </label>
                            <input type="text" class="form-control" disabled>
                        </div>
                        <!-- {{Form::file('document',null, ['class'=>'form-control'])}} -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Email
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('email', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Role
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div> -->
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Role</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('role_id', $roles , (isset($role_of_user) ? $role_of_user : null), ['placeholder'=>'--Select--','class'=>'form-control sumoSelect col-md-7 col-xs-12'])}}
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm user</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('confirmed', [true=>"Yes",false=>"No"] , (isset($posts->user_signin_details) ? $posts->user_signin_details->confirmed : false), ['placeholder'=>'--Select--','class'=>'form-control sumoSelect col-md-7 col-xs-12'])}}
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Phone Number
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('phone_number', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Phone Number Secondary
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('phone_number_secondary', (isset($posts->user_details) ? $posts->user_details->phone_number_secondary : null) ,['class' => 'form-control'] ) !!}
                        </div>
                    </div> 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Address
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('address', (isset($posts->user_details->address) ? $posts->user_details->address : null) ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date of Birth
                        </label>
                        <div class="col-md-6 col-sm-7 col-xs-12">                                       
                            <fieldset>
                                <div class="control-group">
                                    <div class="controls">
                            
                                          {{ Form::date('dob', (isset($posts->user_details) ? $posts->user_details->dob : null), ['class' => 'form-control has-feedback-left' , 'aria-describedby'=>'inputSuccess2Status4','id'=>'datepick-all']) }}

                                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                        <span id="inputSuccess2Status4" class="sr-only">(success)</span>
                                          
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Active</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('active',  ['1'=>'Yes','0'=>'No'], null, ['placeholder'=>'--Select--','class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
                        </div>
                    </div> 
                    <br>
                    <br>
                    <div class="panel panel-default">
                        <div class="panel-heading">  Change Paswword </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                                <div class="col-md-6 col-sm-7 col-xs-12">
                                {{ Form::password('password', null ,['class' => 'form-control col-md-7 col-xs-12'] ) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirm Password</label>
                                <div class="col-md-6 col-sm-7 col-xs-12">
                                {{ Form::password('confirm_password', null ,['class' => 'form-control col-md-7 col-xs-12'] ) }}
                                </div>
                            </div> 
                        </div>
                    </div> 
                    <div class="panel panel-default">
                        <div class="panel-heading">  UG Credit Details </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                                UG Credit Amount (Rs.)
                                </label>
                                <div class="col-md-6 col-sm-7 col-xs-12">
                                {!! Form::number('credit_amount', (isset($posts->user_details) ? $posts->user_details->credit_amount : null) ,['class' => 'form-control'] ) !!}
                                </div>
                            </div>
                        </div>
                    </div>