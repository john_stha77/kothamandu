<?php
namespace Modules\Users;
use Core\Repository\CoreRepository;
use Modules\Users\UsersModel;
use Modules\Users\UsersViewModel;
class UsersRepository extends CoreRepository{
    public function __construct(UsersModel $usersModel){
        $this->usersModel = $usersModel;
        parent::__construct($usersModel);
    }
    public function passwordStrengthFilter($password){
        $check=false;
        if(strlen($password) >= 8){
            $check= true;
        }
        return $check; 
    }
     
     
}