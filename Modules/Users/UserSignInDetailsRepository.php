<?php
namespace Modules\Users;
use Core\Repository\CoreRepository;
use Modules\Users\UserSignInDetailsModel; 
class UserSignInDetailsRepository extends CoreRepository{
    public function __construct(UserSignInDetailsModel $UserSignInDetailsModel){
        $this->UserSignInDetailsModel= $UserSignInDetailsModel;
        parent::__construct($UserSignInDetailsModel);
    }
    public function getOneByUserId($user_id){
        return  $this->UserSignInDetailsModel->where('user_id',$user_id)->first();
    }
    public function updateByUserId($inputData,$user_id){
		return $this->UserSignInDetailsModel->where("user_id",$user_id)->update($inputData);
	}
    public function checkIfExists($user_id){
        $exists=false;
        if($this->getOneByUserId($user_id)){
            $exists=true;
        }
        return $exists;
    }
}