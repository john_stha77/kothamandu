<?php

namespace Modules\Users; 

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class UsersModel extends Authenticatable 
{	
	public $table = "users";
    protected $guarded = ['id','role_id','confirm_password','icon'];

    public function role()
    {
        return $this->belongsTo('Modules\Users\RolesUsersModel', 'user_id');  
    }
    public function user_details(){
    	return $this->hasOne('Modules\Users\UserDetailsModel','user_id');  
    }
    public function user_signin_details(){
    	return $this->hasOne('Modules\Users\UserSignInDetailsModel','user_id');  
    }
}