<?php

namespace Modules\Users;
use Illuminate\Database\Eloquent\Model;

class RolesUsersModel extends Model 
{	
	public $table = "roles_users";
    protected $guarded = ['id'];
}