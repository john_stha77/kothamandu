<?php
namespace Modules\Users;
use Core\Repository\CoreRepository;
use Modules\Users\UserDetailsModel; 
class UserDetailsRepository extends CoreRepository{
    public function __construct(UserDetailsModel $userDetailsModel){
        $this->userDetailsModel= $userDetailsModel;
        parent::__construct($userDetailsModel);
    }
    public function getOneByUserId($user_id){
        return  $this->userDetailsModel->where('user_id',$user_id)->first();
    } 
    public function updateByUserId($inputData,$user_id){
		return $this->userDetailsModel->where("user_id",$user_id)->update($inputData);
	}
    public function checkIfExists($user_id){
        $exists=false;
        if($this->getOneByUserId($user_id)){
            $exists=true;
        }
        return $exists;
    }

}