<?php
namespace Modules\Users;
use Core\Repository\CoreRepository;
use Modules\Users\RolesUsersModel; 
class RolesUsersRepository extends CoreRepository{
    public function __construct(RolesUsersModel $rolesUsersModel){
        $this->rolesUsersModel= $rolesUsersModel;
        parent::__construct($rolesUsersModel);
    }
    public function getOneByUserId($user_id){
        return  $this->rolesUsersModel->where('user_id',$user_id)->first();
    }
    public function destroyByUserId($user_id){
    	return  $this->rolesUsersModel->where('user_id',$user_id)->delete();
    }
}