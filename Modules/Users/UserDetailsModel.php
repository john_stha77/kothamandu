<?php

namespace Modules\Users;
use Illuminate\Database\Eloquent\Model;

class UserDetailsModel extends Model 
{	
	public $table = "user_details";
    protected $guarded = ['id'];
}