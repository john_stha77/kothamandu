<?php

namespace Modules\Users;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Users\UsersRepository;
use Modules\Users\RolesUsersRepository;
use Modules\Roles\RolesRepository;
use Modules\Users\UserSignInDetailsRepository;
use Modules\Users\UserDetailsRepository;
use Modules\Users\UsersRequest;
use Modules\Misc\FileUpload\FileUploadController;

class UsersController extends Controller
{
    private $usersRepo;
    public function __construct(UsersRepository $usersRepo,RolesUsersRepository $rolesUsersRepo,RolesRepository $rolesRepo,UserSignInDetailsRepository $userSignInDetailsRepo,UserDetailsRepository $userDetailsRepo){
        $this->usersRepo =  $usersRepo;
        $this->rolesUsersRepo =  $rolesUsersRepo;
        $this->rolesRepo= $rolesRepo;
        $this->userDetailsRepo =  $userDetailsRepo;
        $this->userSignInDetailsRepo =  $userSignInDetailsRepo;

        $this->iconPath= '/files/users/';
        $this->filepath= '/files/users/';
    } 
     
    public function index()
    {
        $iconPath= $this->iconPath;
        $rolename=array();
        $posts = $this->usersRepo->getPaginate(10,['role','user_details']) ; 
        // foreach ($posts as $p) {
        //      $roles= $this->rolesUsersRepo->getOneByUserId($p->id);
        //      if($roles){ 
        //          $role_id= $roles->role_id; 
        //          $role= $this->rolesRepo->getOneByUserId($role_id);
        //          if($role){
        //             $rolename[$p->id]= $role->name;
        //          }
                 
        //      } 
        // } 
         
        return view('Users.views.index', compact('posts','iconPath'));
    }
    public function create()
    { 
        $roles= $this->rolesRepo->pluck("name","id");
        return view('Users.views.create', compact("roles"));
    }
    public function store(UsersRequest $request)
    {
        if($request['password'] != $request['confirm_password']){
            return redirect()->route('user.create')->withFlashSuccess('Passwords donot match.');
        }
        else{ 
            $data=array();
            $data1=array();
            $user=array();
            $user1=array();
            $user2=array(); 

            if(isset($request['password'])){
                $strengthCheck= $this->usersRepo->passwordStrengthFilter($request['password']);
                if($strengthCheck==false){
                    return redirect()->route('user.create')->withFlashSuccess('Passwords is too weak, please enter stronger password.');
                }
            } 

            $user['name']= $request['name'];
            $user['phone_number']= $request['phone_number'];
            $user['password']= bcrypt($request['password']);
            $user['active']= $request['active'];
            $user['email']= $request['email']; 
            $users = $this->usersRepo->store($user);
            //icon upload
            $file= $request->file('icon');
            $filepath= '/files/users/';
            $data=array();
            $uploadInstance= new FileUploadController();
            $upload= $uploadInstance->file_upload($file, $filepath, $watermark=false, $resize=true);
            if($upload)
            {
                $data['avatar_file_name']=  $upload['file_name'];
                $data['avatar_file_size']= $upload['file_size'];
                $data['avatar_content_type'] = $upload['content_type']; 
            } 
            //end icon upload 
            $user1['user_id']= $users->id; 
            $user1['phone_number_secondary']= $request['phone_number_secondary'];
            $user1['address']= $request['address'];
            $user1['dob']= $request['dob']; 
            $user1['credit_amount']= $request['credit_amount'];
            $user1['avatar_file_name']= isset($data['avatar_file_name']) ? $data['avatar_file_name'] : "";
            $user1['avatar_content_type']= isset($data['avatar_file_name']) ? $data['avatar_content_type'] : "";
            $user1['avatar_file_size']= isset($data['avatar_file_name']) ? $data['avatar_file_size'] : 0; 
            $usersDetails = $this->userDetailsRepo->store($user1);
            //if confirmation is false generate a confirmation token
            if(($request['confirmed']==false) || ($request['confirmed']==null)){
                $user2['confirmation_token']= str_random(30);
                $user2['confirmed']= null;
            }
            $usersSignInDetails = $this->userSignInDetailsRepo->store($user2); 
            //add roles of the user 
            if($users){
                $data1['user_id']= $users->id;
                $data1['role_id']= $request['role_id'];
                 $this->rolesUsersRepo->store($data1);
            }
            return redirect()->route('user.index')->withFlashSuccess('Users has been added successfully.');
        }
       

        return redirect()->route('users.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $role_of_user=null;
        $roles = $this->rolesRepo->pluck('name','id');
        $get_role= $this->rolesUsersRepo->getOneByUserId($id);
        if($get_role){ 
            $role_of_user= $get_role->role_id;
        }
        $roles->prepend('Null', "");
        
        $posts= $this->usersRepo->getById($id); 
        return view('Users.views.edit', compact('posts','roles','role_of_user'));
    }
    public function update(UsersRequest $request, $id)
    {

        if($request['password'] != $request['confirm_password']){
            return redirect()->route('users.edit', $id)->withFlashSuccess('Passwords donot match.');
        }
        else{ 
            $data=array();
            $data1=array();
            $user=array();
            $user1=array();
            $user2=array();
 
            //if password is null donot save it in users table
            //if password is not null bcrypt it 
            if($request['password']!=""){
                $strengthCheck= $this->usersRepo->passwordStrengthFilter($request['password']);
                if($strengthCheck==false){
                    return redirect()->route('users.edit', $id)->withFlashSuccess('Passwords is too weak, please enter stronger password.');
                }
                $user['password']= bcrypt($request['password']);
            } 
            $user['name']= $request['name'];
            $user['phone_number']= $request['phone_number']; 
            $user['active']= $request['active'];
            $user['email']= $request['email']; 
            $users = $this->usersRepo->update($user,$id); 
            $filepath= $this->filepath; 
            if($request->hasFile('icon'))
            {   
                //delete previous icon
                $p_icon = $this->usersRepo->getById($id);
                $file_deleted= fileDeleteIfExist($filepath,$p_icon->avatar_file_name);
            }
            //icon upload
            $file= $request->file('icon');
            $uploadInstance= new FileUploadController();

            $upload= $uploadInstance->file_upload($file, $filepath, $watermark=false, $resize=true);
            if($upload)
            {
                $data['avatar_file_name']=  $upload['file_name'];
                $data['avatar_file_size']= $upload['file_size'];
                $data['avatar_content_type'] = $upload['content_type']; 
                $data['avatar_updated_at']= getCurrentTime();
            } 
            //end icon upload  
            $user1['user_id']= $id; 
            $user1['phone_number_secondary']= $request['phone_number_secondary'];
            $user1['address']= $request['address'];
            $user1['dob']= $request['dob']; 
            $user1['credit_amount']= $request['credit_amount'];
            if($upload){
                $user1= array_merge($user1,$data);
            } 

            //some time data in user details and user signin details are not saved if empty
            //update user details if exists or store it
            $usersDetails = ($this->userDetailsRepo->checkIfExists($id)==true) ? $this->userDetailsRepo->updateByUserId($user1,$id) : $this->userDetailsRepo->store($user1); 
            //if confirmation is false generate a confirmation token  
            if(($request['confirmed']==false) || ($request['confirmed']==null)){
                $user2['confirmation_token']=  str_random(30);
                $user2['confirmed']= null;
            } 
            else{
                $user2['confirmed']= true;
            }
            $user2['user_id']=$id;
            //some time data in user signin details and user signin details are not saved if empty
            //update user signin details if exists or store it
            $usersSignInDetails = ($this->userSignInDetailsRepo->checkIfExists($id)==true) ? $this->userSignInDetailsRepo->updateByUserId($user2,$id) : $this->userSignInDetailsRepo->store($user2); 
            if($users){
                //delete all
                $this->rolesUsersRepo->destroyByUserId($id);
                //create new 
                if($request['role_id'] ){
                    $data1['user_id']= $id;
                    $data1['role_id']= $request['role_id'];
                    $this->rolesUsersRepo->store($data1);
                }
                return redirect()->route('users.edit', $id)->withFlashSuccess('Users has been edited successfully.');
            }
        }
        

        return redirect()->route('users.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->usersRepo->destroy($id); 
        $usersDetailsId= $this->usersDetailsRepo->getOneByUserId($id)->id;
        $this->userDetailsRepo->destroy($id);
        $usersSignInDetails= $this->usersSignInDetails->getOneByUserId($id)->id;
        $this->usersSignInDetails->destroy($id);
        return redirect()->route('users.index')->withFlashSuccess('It is deleted successfully.');
    } 
}