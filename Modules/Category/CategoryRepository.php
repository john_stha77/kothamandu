<?php
namespace Modules\Category;
use Core\Repository\CoreRepository;
use Modules\Category\CategoryModel;
use Modules\Category\CategoryViewModel;
class CategoryRepository extends CoreRepository{
    public function __construct(CategoryModel $categoryModel){
        parent::__construct($categoryModel);
    }
    public function getAllCategoriesByLayers($paginate=false)
    { 
        $category_ids=array();
        $subcategory_ids=array();
        $subsubcategory_ids=array();

        $subcategory=array();
        $subsubcategory=array();



       if($paginate!=false){
           if(gettype($paginate)=="integer"){
                $category= CategoryModel::where('parent_id', null)->paginate($paginate); 
                $category_ids=array();
                foreach ($category as $c) {
                    $category_ids[]=$c->id;
                }
           }
           
       }
       else{
            $category= CategoryModel::where('parent_id', null)->get();
       } 
        if($category){
            foreach ($category as $c) { 
                $subcategory[$c->id]= CategoryModel::where('parent_id', $c->id)->get();
                foreach ($subcategory[$c->id] as $s) {
                    $subsubcategory[$c->id][$s->id]= CategoryModel::where('parent_id', $s->id)->get();
                    // foreach ($subsubcategory[$c->id][$s->id] as $ss) {

                    //     $subsubcategory_ids[]= $ss->id;
                    //     $subsubsubcategory[$c->id][$s->id][$ss->id]= Category::where('parent_id', $ss->id)->get();
                    // }
                }
            }
        }   
        
        return  compact('category','subcategory','subsubcategory');
    }
}