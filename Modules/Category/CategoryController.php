<?php

namespace Modules\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Category\CategoryRepository; 
use Modules\Category\CategoryRequest;
use Modules\Misc\FileUpload\FileUploadController;
use Carbon\Carbon;

class CategoryController extends Controller
{
    private $categoryRepo;
    public function __construct(CategoryRepository $categoryRepo){
        $this->categoryRepo =  $categoryRepo;
        $this->filepath= '/files/category/';
    } 


     
    public function index()
    {
        // $posts = $this->categoryRepo->getPaginate() ; 
        $categoryLayers= $this->categoryRepo->getAllCategoriesByLayers(2);
        $category= $categoryLayers['category'];
        $subcategory= $categoryLayers['subcategory'];
        $subsubcategory= $categoryLayers['subsubcategory'];
         
        return view('Category.views.index', compact('category','subcategory','subsubcategory'));
    }
    public function create()
    {   
        $category= array();
        $cats = $this->categoryRepo->getAll();
        
        foreach ($cats as $c) {
            $category[$c->id]= $c->name;
        }
        $category['null']= "-Remove as child-";
        return view('Category.views.create',compact("category"));
    }
    public function store(CategoryRequest $request)
    {   
        if($request['parent_id']== "null"){
            $request['parent_id']= null;
        }
        $file= $request->file('icon');
        $filepath= $this->filepath;
        
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=true, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type']; 
        } 
        $category = $this->categoryRepo->store($request->all());

        return redirect()->route('category.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $category= array();
        $cats = $this->categoryRepo->getAll();
        
        foreach ($cats as $c) {
            $category[$c->id]= $c->name;
        }
        $category['null']= "-Remove as child-";
        $posts= $this->categoryRepo->getById($id);
        
        return view('Category.views.edit', compact('posts','category'));
    }
    public function update(CategoryRequest $request, $id)
    {   
        if($request['parent_id']== "null"){
            $request['parent_id']= null;
        }
        $category = $this->categoryRepo->getById($id);
        $filepath= $this->filepath;
        if($request->hasFile('icon'))
        {   
            //delete previous icon 
            $file_deleted= fileDeleteIfExist($filepath,$category->image_file_name);
        }   
        $file= $request->file('icon'); 
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=false, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type'];
            $request['image_updated_at']= Carbon::now(getCurrentTimeZone()); 
        } 
        $category= $this->categoryRepo->update($request->all(),$id);

        return redirect()->route('category.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->categoryRepo->destroy($id); 
        return redirect()->route('category.index')->withFlashSuccess('It is deleted successfully.');
    } 
}