<div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Name
                        <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Slug
                        <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::text('slug', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Parent</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('parent_id', $category , null, ['placeholder'=>'--Select Parent--','class'=>'form-control sumoSelect col-md-7 col-xs-12'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Active</label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {{Form::select('active', ['1'=>'Yes','0'=>'No'] , null, ['placeholder'=>'--Select--','class'=>'form-control sumoSelect col-md-7 col-xs-12' ])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation">Category Image </label>
                        <div class="col-md-6 col-sm-7 col-xs-12"> 
                            @if(isset($posts))
                            @if($posts->image_file_name !=null)
                                <img class="img-thumbnail" src="{{asset('files/category/small/'.$posts->image_file_name)}}"  height="150" width="150">
                            @endif
                            @endif
                            Browse {{Form::file('icon',null, ['class'=>'form-control'])}} 
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                        Description
                        <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-7 col-xs-12">
                        {!! Form::textarea('description', null ,['class' => 'form-control'] ) !!}
                        </div>
                    </div>