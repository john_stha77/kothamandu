@extends('layouts.backend.master')

@section('content')

<div class="x_panel">

        <div class="x_title">
            <h2>Category</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <span><a href="{!! route('category.create') !!}" class="btn btn-primary">Add New  Category</a>
                    </span> 
                </li>
            </ul>  
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Description</th>                 
                     
                    <th>Action</th>

                </tr>
                </thead>

                <tbody>
                     
                @foreach($category as $c)
                <tr> 
                    <td>
                        @if($c->image_file_name !=null)
                            <img class="img-thumbnail" src="{{asset('files/category/small/'.$c->image_file_name)}}"  height="150" width="150">
                        @endif
                    </td>   
                    <td>{{$c->name}}</td>
                    <td>{{$c->description}} </td>
                    <td>
                   
                    <a href=" {{ route('category.edit',$c->id) }}" class="action-btns">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    {{Form::open(['route'=>['category.destroy', $c->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                        <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                    {{Form::close()}}
                   
                    </td>
                </tr>
                    @foreach($subcategory[$c->id] as $s)
                    <tr>   
                        <td>
                        @if($s->image_file_name !=null)
                            <img class="img-thumbnail" src="{{asset('files/category/small/'.$s->image_file_name)}}"  height="150" width="150">
                        @endif
                    </td>    
                        <td>-{{$s->name}}</td>
                        <td>{{$s->description}} </td>
                        <td>
                   
                        <a href=" {{ route('category.edit',$s->id) }}" class="action-btns">
                            <span class="glyphicon glyphicon-pencil"></span>
                        </a>
                        {{Form::open(['route'=>['category.destroy', $s->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                            <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                        {{Form::close()}}
                       
                        </td>
                    </tr> 
                        @foreach($subsubcategory[$c->id][$s->id] as $ss)
                            <tr> 
                                <td>
                                    @if($ss->image_file_name !=null)
                                        <img class="img-thumbnail" src="{{asset('files/category/small/'.$ss->image_file_name)}}"  height="150" width="150">
                                    @endif
                                </td>      
                                <td>--{{$ss->name}}</td>
                                <td>{{$ss->description}} </td>
                                <td>
                   
                                <a href=" {{ route('category.edit',$ss->id) }}" class="action-btns">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                {{Form::open(['route'=>['category.destroy', $ss->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                                    <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                                {{Form::close()}}
                               
                                </td>
                            </tr> 
                            {{-- 
                            @foreach($subsubsubcategory[$c->id][$s->id][$ss->id] as $sss)
                            <tr>  
                                <td>
                                    @if($sss->image_file_name !=null)
                                        <img class="img-thumbnail" src="{{asset('files/category/small/'.$sss->image_file_name)}}"  height="150" width="150">
                                    @endif
                                </td>  
                                <td>---{{$sss->name}}</td>
                                <td>{{$sss->description}} </td>
                                <td>
                   
                                <a href=" {{ route('category.edit',$sss->id) }}" class="action-btns">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                {{Form::open(['route'=>['category.destroy', $sss->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                                    <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                                {{Form::close()}}
                               
                                </td>
                            </tr>  
                            @endforeach 
                            --}}
                        @endforeach

                    @endforeach
                @endforeach
                </tbody>
            </table>
            {{ $category->links() }} 
        </div>
    </div>


@endsection