<?php

namespace Modules\Category;
use Core\Model\CoreModel; 

class CategoryModel extends CoreModel 
{	
	public $table = "category";
    protected $guarded = ['id','icon'];
}