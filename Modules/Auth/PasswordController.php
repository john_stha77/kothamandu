<?php

namespace Modules\Auth;

use App\Http\Controllers\Controller; 
use Modules\Misc\Email\EmailSendController;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Modules\Users\UsersModel;
use Modules\Users\UserSignInDetailsModel;

class PasswordController extends Controller
{
     
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function viewEmailField(){
        return view('Auth.password-reset.email');
    }
    public function postEmailField(Request $request){
        $email= $request['email'];
        $passwordResetToken=  str_random(30);
        $now= Carbon::now(getCurrentTimeZone()); 
        
        //add data to users tables
        $user= UsersModel::where("deleted_at",null)->where("email",$email)->first();
        $userSignIn= UserSignInDetailsModel::where("user_id",$user->id)->first();
        //if exists update else create sign in details
        if($userSignIn){
            $userSignIn->update([ 
                'reset_password_token' => $passwordResetToken,
                'reset_password_sent_at' => $now, 
            ]);
        }
        else{
            UserSignInDetailsModel::create([
                'user_id' => $user->id,
                'reset_password_token' => $passwordResetToken,
                'reset_password_sent_at' => $now, 
            ]);
        }
        //send password reset link to this email
        $email_sent= EmailSendController::sendPasswordResetLink($email, $passwordResetToken); 
        if(!$email_sent){
             return back()->withErrors("Sorry, there is a problem in sending a password reset link to your email.");
  
        }
        return back()->withFlashSuccess("We have sent a password reset link to your email. Please check your email.");

    }

    public function getPasswordRequest($email,$passwordResetToken){
        //validate token and expiry date
        //if it validates return to password reset page
        $changePassword= $this->validateResetToken($email,$passwordResetToken );  
            
        return view("Auth.password-reset.change-password",compact('email','changePassword','passwordResetToken'));
    }
    // public function viewPasswordReset(){
    //     return view("frontend.account.password-reset.change-password");
    // }  
    public function postPasswordReset(Request $request){
        $email=$request['email'] ;
        $passwordResetToken= $request['password_reset_token'];
        $password= $request['password'] ;
        $confirm_password= $request['confirm_password'] ;

        $token_validation= $this->validateResetToken($email,$passwordResetToken );
        
        if(!$token_validation){ 
            return back()->withErrors("Sorry, Your password reset code is invalid. Please try again.");
        } 
        if($password != $confirm_password){ 
            return back()->withErrors("Sorry, Confirm password donot match.");
        } 
        $newPassword = bcrypt($password);
        $check= UsersModel::where('email',$email)->update(['password'=>$newPassword]);    
        
        //login user after password reset
        $credentials = [
            'email' => $email,
            'password' => $password,
            'confirmed' => true ,
            'deleted_at' => null,
        ];
        $userDetail= UsersModel::with("user_signin_details")->where('deleted_at',null)->where('email',$email)->first();
        if($userDetail){
            if($userDetail->user_signin_details->confirmed !=true){
                //send confirmation email
                $user['name']= $userDetail->name;
                $user['email']= $userDetail->email;
                // EmailSendController::sendConfirmation($user, $userDetail->confirmation_token);
                $url= url('resendConfirmationCode/'.$user['email']); 
                $msg="We have sent you a confirmation email. Please confirm it.<br><br>";
                $msg .= "<u><a href='".$url."'><strong>Resend Confirmation Code</strong></a></u>";
                return redirect('login')->withErrors($msg);
            }
        }
        $logged= Auth::attempt($credentials); 
        if(!$logged){
            return redirect('login')->withErrors("Sorry, an error occured. Please try again.");
        } 
        
        return redirect("/");
    }
    public function validateResetToken($email,$resetTokenByUser ){
        $check= false;
        //get user details of that email
        $userDetails= UsersModel::with("user_signin_details")->where("deleted_at",null)->where("email",$email)->first();  
        $reset_password_token=$userDetails->user_signin_details->reset_password_token;
        $reset_password_sent_at= $userDetails->user_signin_details->reset_password_sent_at;
        //now validate token 
        if($resetTokenByUser==$reset_password_token){ 
            $timezone=getCurrentTimeZone();
            $now= Carbon::now($timezone); 
            $expiry_time= Carbon::createFromFormat("Y-m-d H:i:s", $reset_password_sent_at, $timezone); 
            $expiry_time->addDays(2);
            //if expiry time is greater than current time
            //password reset token is valid
            //and let user to change password
            
            if($expiry_time->gt($now)){
                $check= true;
            }  
        }  
        return $check;
    }


}



