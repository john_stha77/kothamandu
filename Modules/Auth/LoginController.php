<?php

namespace Modules\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Modules\Users\UsersModel;
use Modules\Users\UsersRepository;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UsersRepository $usersRepo)
    {
        $this->usersRepo =  $usersRepo;
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect('/');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {   
        $userSocial = Socialite::driver('facebook')->user();
       //dd($userSocial);

        $finduser=UsersModel::orwhere('email',$userSocial->email, 'social_id',$userSocial->id)->first();
        if ($finduser){   
            //dd($finduser);
            Auth::login($finduser); 
            return redirect('admin');
        }
        else {    
            $user['social_id']=$userSocial->id;
            $user['name']=$userSocial->name;
            $user['email']=$userSocial->email;
            $this->usersRepo->store($user);
            return redirect('admin');
            
        }

       
    }

}
