@extends('layouts.frontend.master')
@section('content')
 



<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <span class="pull-right"><a href="{{url('/')}}">Home</a> / Password Reset</span>
    <h2>Password Reset</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="spacer">
<div class="row register">
    @include('Misc.Errors.errors.error-success')

    <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 "> 
        <h3>Password Reset</h3>
        <form method="POST" action="{{ route('postEmailField') }}">
            {{ csrf_field() }}
            <input type="email" class="form-control" placeholder="Email Address" name="email"> 
            <button type="submit" class="btn btn-success" name="Submit">Send Password reset link</button> 
        </form>
    </div>
  
</div>
</div>
</div>

@endsection