@extends('layouts.frontend.master')
@section('content')
  
<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <span class="pull-right"><a href="{{url('/')}}">Home</a> / Password Reset</span>
    <h2>Password Reset</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="spacer">
<div class="row register">
    @include('Misc.Errors.errors.error-success')
    
    <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 "> 
        <h3>Password Reset</h3>
        @if($changePassword)
        <form method="POST" action="{{ route('postPasswordReset') }}">
			{{ csrf_field() }}
			<input type="hidden" name="email" value="{{$email}}">
			<input type="hidden" name="password_reset_token" value="{{$passwordResetToken}}"> 
			<input type="password" name="password" required class="form-control" id="exampleInputPassword1" placeholder="Password"> 
			<input type="password" name="confirm_password" required class="form-control" id="exampleInputPassword1" placeholder="Confirm password"> 
			 
			<button type="submit" class="btn btn-primary">Change Password</button>
		</form>
		@else
		<h3>Sorry, Your password reset code is invalid. Please try again.</h3>
		@endif
	</div> 
  
</div>
</div>
</div>
@endsection