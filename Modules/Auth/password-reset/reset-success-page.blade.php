@extends('layouts.frontend.master')
@section('content')
<div id="main" class="container">
	<div class="row"> 
		<div class="col-md-6"> 

			<h3>Your password reset is successful.</h3>
			<a href="{{url('/)}}" class="btn btn-primary">Continue </a>
			
		</div>
	</div>
</div>
@endsection 