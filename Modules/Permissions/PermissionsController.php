<?php

namespace Modules\Permissions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Permissions\PermissionsRepository;
use Modules\Permissions\PermissionsInfoRepository;
use Modules\Permissions\PermissionsRequest;

class PermissionsController extends Controller
{
    private $permissionsRepo;
    public function __construct(PermissionsRepository $permissionsRepo){
        $this->permissionsRepo =  $permissionsRepo;

    } 
     
    public function index()
    {
        $posts = $this->permissionsRepo->getPaginate() ; 
         
        return view('Permissions.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Permissions.views.create');
    }
    public function store(PermissionsRequest $request)
    {
        $permissions = $this->permissionsRepo->store($request->all());

        return redirect()->route('permissions.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->permissionsRepo->getById($id);
        
        return view('Permissions.views.edit', compact('posts'));
    }
    public function update(PermissionsRequest $request, $id)
    {
        $permissions= $this->permissionsRepo->update($request->all(),$id);

        return redirect()->route('permissions.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->permissionsRepo->destroy($id); 
        return redirect()->route('permissions.index')->withFlashSuccess('It is deleted successfully.');
    } 
}