<?php
namespace Modules\Permissions;
use Core\Repository\CoreRepository;
use Modules\Permissions\PermissionsModel;
use Modules\Permissions\PermissionsViewModel;
class PermissionsRepository extends CoreRepository{
    public function __construct(PermissionsModel $permissionsModel){
        parent::__construct($permissionsModel);
    }
}