<?php

namespace Modules\Permissions;
use Core\Model\CoreModel; 

class PermissionsModel extends CoreModel 
{	
	public $table = "permissions";
    protected $guarded = ['id','role_id'];
}