<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Name
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
    </div>
</div>

<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type"> 
      Code
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
         {!! Form::text('code', null ,['class' => 'form-control'] ) !!}
    </div>
</div>