<?php

namespace Modules\ApplyProperty;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\ApplyProperty\ApplyPropertyRepository; 
use Modules\ApplyProperty\ApplyPropertyRequest;
use Auth;

class ApplyPropertyController extends Controller
{
    private $applypropertyRepo;
    public function __construct(ApplyPropertyRepository $applypropertyRepo){
        $this->applypropertyRepo =  $applypropertyRepo;

    } 
     
    public function index()
    {
        $with= ['property','user'];
        $posts = $this->applypropertyRepo->getPaginate(10,$with) ; 
         
        return view('ApplyProperty.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('ApplyProperty.views.create');
    }
    public function store(ApplyPropertyRequest $request)
    {
        $request['created_by']= Auth::user()->id;
        $applyproperty = $this->applypropertyRepo->store($request->all());

        return redirect()->route('applyproperty.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->applypropertyRepo->getById($id);
        
        return view('ApplyProperty.views.edit', compact('posts'));
    }
    public function update(ApplyPropertyRequest $request, $id)
    {
        $request['updated_by']= Auth::user()->id;
        $applyproperty= $this->applypropertyRepo->update($request->all(),$id);

        return redirect()->route('applyproperty.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->applypropertyRepo->destroy($id); 
        return redirect()->route('applyproperty.index')->withFlashSuccess('It is deleted successfully.');
    } 
}