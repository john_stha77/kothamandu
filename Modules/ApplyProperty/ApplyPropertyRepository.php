<?php
namespace Modules\ApplyProperty;
use Core\Repository\CoreRepository;
use Modules\ApplyProperty\ApplyPropertyModel;
use Modules\ApplyProperty\ApplyPropertyViewModel;
class ApplyPropertyRepository extends CoreRepository{
    public function __construct(ApplyPropertyModel $applypropertyModel){
        parent::__construct($applypropertyModel);
    }

    public function getAllAppliedPropertyIds($user_id){
    	$applied_property_ids= array();
    	$appliedPropertyDetails= ApplyPropertyModel::where('user_id',$user_id)->get();
    	foreach ($appliedPropertyDetails as $a) {
    		 $applied_property_ids[]= $a->property_id;
    	}
    	return $applied_property_ids;
    }
    public function undoApply($data){
    	$user_id= $data['user_id'];
    	$property_id= $data['property_id'];
    	$appliedPropertyDetails= ApplyPropertyModel::where('user_id',$user_id)->where('property_id', $property_id)->delete(); 
    	return back();
    }
}