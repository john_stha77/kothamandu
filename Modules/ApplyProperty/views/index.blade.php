@extends('layouts.backend.master')

@section('content')

<div class="x_panel">

        <div class="x_title">
            <h2>ApplyProperty</h2>
            <ul class="nav navbar-right panel_toolbox">
                <li>
                    <span><a href="{!! route('applyproperty.create') !!}" class="btn btn-primary">Add New  ApplyProperty</a>
                    </span> 
                </li>
            </ul>  
            <div class="clearfix"></div>
        </div>
        
        <div class="x_content"> 
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>S.N.</th> 
                    <th>User</th>
                    <th>Property</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                    <?php  $i=1; ?>
                @foreach($posts as $p) 
                <tr>
                    <td> {{ $i }}</td> 
                    <td> {{isset($p->user_id) ? $p->user->name : ""}} </td>
                    <td> {{isset($p->property_id) ? $p->property->name : ""}} </td>
                    <td> 
                    <a href=" {{ route('applyproperty.edit',$p->id) }}" class="action-btns">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    {{Form::open(['route'=>['applyproperty.destroy', $p->id] , 'method'=>'DELETE', 'class'=>'form-inline' ])}}
                         <a href="javascript:void(0);" class="action-btns submit"><span class="glyphicon glyphicon-trash"></span></a>
                    {{Form::close()}}
                   
                    </td>
                </tr>
                <?php $i++; ?> 
                @endforeach
                
                </tbody>
            </table>
            
            {{ $posts->links() }}
        </div>
    </div>


@endsection