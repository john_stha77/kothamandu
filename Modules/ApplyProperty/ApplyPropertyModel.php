<?php

namespace Modules\ApplyProperty;
use Core\Model\CoreModel; 

class ApplyPropertyModel extends CoreModel 
{	
	public $table = "apply_property";
    protected $guarded = ['id']; 

    public function user(){
    	return $this->belongsTo('Modules\Users\UsersModel', 'user_id'); 
    }
    public function property(){
    	return $this->belongsTo('Modules\Property\PropertyModel', 'property_id'); 
    }
}