<?php

namespace Modules\Newsletter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Newsletter\NewsletterRepository; 
use Modules\Newsletter\NewsletterRequest;

class NewsletterController extends Controller
{
    private $newsletterRepo;
    public function __construct(NewsletterRepository $newsletterRepo){
        $this->newsletterRepo =  $newsletterRepo;

    } 
     
    public function index()
    {
        $posts = $this->newsletterRepo->getPaginate() ; 
         
        return view('Newsletter.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Newsletter.views.create');
    }
    public function store(NewsletterRequest $request)
    {
        $newsletter = $this->newsletterRepo->store($request->all());

        return redirect()->route('newsletter.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->newsletterRepo->getById($id);
        
        return view('Newsletter.views.edit', compact('posts'));
    }
    public function update(NewsletterRequest $request, $id)
    {
        $newsletter= $this->newsletterRepo->update($request->all(),$id);

        return redirect()->route('newsletter.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->newsletterRepo->destroy($id); 
        return redirect()->route('newsletter.index')->withFlashSuccess('It is deleted successfully.');
    } 
}