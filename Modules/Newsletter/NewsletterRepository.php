<?php
namespace Modules\Newsletter;
use Core\Repository\CoreRepository;
use Modules\Newsletter\NewsletterModel;
use Modules\Newsletter\NewsletterViewModel;
class NewsletterRepository extends CoreRepository{
    public function __construct(NewsletterModel $newsletterModel){
        parent::__construct($newsletterModel);
    }
}