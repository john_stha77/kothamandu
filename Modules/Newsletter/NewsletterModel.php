<?php

namespace Modules\Newsletter;
use Core\Model\CoreModel; 

class NewsletterModel extends CoreModel 
{	
	public $table = "newsletter";
    protected $guarded = ['id'];
}