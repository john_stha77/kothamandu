<?php


namespace Modules\Property\ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class Properties extends Controller
{
    /**
     * Show the application layout.
     *
     * @return \Illuminate\Http\Response
     */
    public function layout()
    {
        return view('backend.order.create');
    }


    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAjax(Request $request)
    {
        $data = [];


        if($request->has('q')){
            $search = $request->q;

            $data = DB::table("property")
                    ->select("id","name")
                    ->where('deleted_at',null)
                    ->where('active',true)
                    ->where('name','like',"%$search%")
                    ->orWhere('sku','like',"%$search%") 
                    ->orWhere('keywords','like',"%$search%")                     
                    ->get();
                   
            
            }

        return response()->json($data);
   
    }

}