@extends('layouts.backend.master')

@section('content') 

<div class="x_panel">
            <div class="x_title">
                <h2>Edit Property</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <span><a href="{!! route('property.index') !!}" class="btn btn-primary">Property Index</a></span>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            
            <div class="x_content">
                @include('error-success') 
            
                {{Form::model($posts, ['route'=>['property.update', $posts->id], 'id'=>'demo-form2', 'class'=>'form-horizontal form-label-left','method'=>'patch', 'data-parsley-validate', 'files'=>'true'])}}
               
                    @include('Property.views.form.form')
                     
                    <div class="form-footer">
                        <div class="form-group">
                            <div class="submit-btn col-md-6 col-md-offset-3 col-sm-offset-3">
                                <button type="submit"  class="btn btn-success">Submit</button>
                                <a href="{{ route('property.index') }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </div> 
 
                {{Form::close()}}
            </div>
        </div>








@endsection