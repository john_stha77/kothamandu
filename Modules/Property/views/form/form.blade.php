<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Name
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('name', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    SKU
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('sku', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Category
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12 user_id">
    <select class="category_search form-control" name="category_id">
        @if(isset($posts->category_id))
        <option value="{{$posts->category_id}}">{{$posts->category->name}}</option>
        @endif
    </select>
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Location
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12 user_id">
    <select class="location_search form-control" name="location_id">
        @if(isset($posts->location_id))
        <option value="{{$posts->location_id}}">{{$posts->location->name}}</option>
        @endif
    </select>
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation">Image </label>
    <div class="col-md-6 col-sm-7 col-xs-12"> 
        @if(isset($posts))
        @if($posts->property_images!=null)
        @foreach($posts->property_images as $image)
            <img class="img-thumbnail"  src="{{asset('files/property/'.$posts->id.'/small/'.$image->image_file_name)}}"  height="150" width="150">
        @endforeach
        @endif
        @endif
        Browse  <input type="file" name='icon[]' class="form-control" multiple>  
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Price ( Rs. )
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('price', null ,['class' => 'form-control'] ) !!}
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Sale Price
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('sale_price', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Discountable</label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {{Form::select('discountable',  [1=>'Yes',0=>'No'], null, ['class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Active</label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {{Form::select('active',  [1=>'Yes',0=>'No'], null, ['class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {{Form::select('status',  ['on-sale'=>'On Sale','sold' =>'Sold'], null, ['class'=>'form-control sumoSelect col-md-7 col-xs-12', 'required'])}}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type"> 
      Description of Property
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
        {{Form::textarea('description', null, ['class'=>'form-control ','id'=>'summary-ckeditor','required'])}}
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12 user_search_label" for="first-name">
    User
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12 user_id">
    <select class="user_search form-control"  name="user_id">

        @if(isset($posts->user_id))
        @if($posts->user_id !=null)
            <option value="{{$posts->user_id}}">{{$posts->user->name}}</option>
        @endif
        @endif
    </select>
    </div>
</div>        
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Meta keywords
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('meta_keywords', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
     Meta description
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('meta_description', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type"> 
      Keywords
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
        {{Form::textarea('keywords', null, ['class'=>'form-control '])}}
    </div>
</div> 

