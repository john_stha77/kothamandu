<?php

namespace Modules\Property;
use Core\Model\CoreModel; 

class PropertyModel extends CoreModel 
{	
	public $table = "property";
    protected $guarded = ['id','icon'];

    public function user(){
    	return $this->belongsTo('app\User', 'user_id'); 
    }
    public function property_images(){
        return $this->hasMany('Modules\Property\PropertyImageModel', 'property_id'); 
    }
    public function category(){
    	return $this->belongsTo('Modules\Category\CategoryModel', 'category_id'); 
    }
    public function location(){
    	return $this->belongsTo('Modules\Locations\LocationsModel', 'location_id'); 
    }
}