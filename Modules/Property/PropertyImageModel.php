<?php

namespace Modules\Property;
use Illuminate\Database\Eloquent\Model;

class PropertyImageModel extends Model 
{	
	public $table = "property_images";
    protected $guarded = ['id'];
}