<?php
namespace Modules\Property;
use Core\Repository\CoreRepository;
use Modules\Property\PropertyModel;
use Modules\Property\PropertyViewModel;
use Modules\Property\PropertyImageModel;
class PropertyRepository extends CoreRepository{
    public function __construct(PropertyModel $propertyModel,PropertyImageModel $propertyImageModel){
        parent::__construct($propertyModel);
        $this->propertyImageModel= $propertyImageModel;
        $this->propertyModel= $propertyModel;
    }
    public function storeImageDetails($inputData){
    	return $this->propertyImageModel->create($inputData);
    }
    public function updateImageDetails($inputData){
    	return $this->propertyImageModel->create($inputData);
    }
    public function destroyImageDetails($id){
    	return  $this->propertyImageModel->where('property_id',$id)->delete();
    }
    public function getCategoryByPropertyId($property_id){
        $category = $this->propertyModel->where('id',$property_id)->first();
        if(!$category){
            return null;
        }
        return $category->category_id;
    }
    public function getPropertiesByCategoryId($category_id){ 
        $products = $this->propertyModel->where('active', true)->where('category_id', $category_id)->get();  
       
        return $products;
    }
    public function getAllOrderBy($orderBy,$with=null){
        $result=null;
        foreach ($orderBy as $key => $value) { 
                $result= ($with==null) ? $this->propertyModel->orderBy($key,$value)->get() : $this->propertyModel->with($with)->orderBy($key,$value)->get(); 
            break;
        }
        
        return $result; 
    }
    public function getOrderBy($count,$orderBy,$with=null){
        $result=null;
        foreach ($orderBy as $key => $value) { 
                $result= ($with==null) ? $this->propertyModel->orderBy($key,$value)->limit($count)->get() : $this->propertyModel->with($with)->orderBy($key,$value)->limit($count)->get(); 
            break;
        }
        
        return $result; 
    }
    public function getPostedPropertiesByUserId($user_id){
        $properties= $this->propertyModel->where("user_id",$user_id)->get();
        return $properties;
    }
    public function hot($count){
        $data = $this->propertyModel->orderBy('view_count','DESC')->get()->take($count);
        return $data;
    }
}