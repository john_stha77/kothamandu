<?php

namespace Modules\Property;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Property\PropertyRepository; 
use Modules\Property\PropertyRequest;
use Illuminate\Support\Facades\Storage;
use Modules\Misc\FileUpload\FileUploadController;
use File;

class PropertyController extends Controller
{
    private $propertyRepo;
    public function __construct(PropertyRepository $propertyRepo){
        $this->propertyRepo =  $propertyRepo;
        $this->filepath= "/files/property/";
        $this->imageCountAllowed= 3;

    } 
     
    public function index()
    {
        $posts = $this->propertyRepo->getPaginate(10,['location','category']) ; 
         
        return view('Property.views.index', compact('posts'));
    }
    public function create()
    { 
        return view('Property.views.create');
    }
    public function store(PropertyRequest $request)
    {   
        $property = $this->propertyRepo->store($request->all());

        //now add all images
        if($request->hasFile('icon')){ 
            
            $countReferencePhoto=0; 
            $filepath= $this->filepath.$property->id.'/'; 

            //now delete previous files if exists
            $public_disk= Storage::disk('public');
            $filenames = $public_disk->files($filepath);  
            foreach($filenames as $filename){  
                if(is_file(public_path()."/".$filename)){  
                    $public_disk->delete($filename);  
                }
            }
            //reset image details for adding session 
            $image_file_name= array();
            $image_file_size= array();
            $image_content_type= array(); 
            foreach ($request->file('icon') as $file) { 
                if(!in_array($file->getClientOriginalExtension(), ['jpg','png','jpeg'])){
                    return back()->withErrors("Invalid file uploaded. Please upload image file.");
                }
                //restrict to value of imageCountAllowed
                if($countReferencePhoto < $this->imageCountAllowed){  
                    $uploadInstance= new FileUploadController();
                    $upload= $uploadInstance->file_upload($file, $filepath, $watermark=false, $resize=true);
                    if($upload){  
                        $image_file_name[]=  $upload['file_name'];
                        $image_file_size[]= $upload['file_size'];
                        $image_content_type[] = $upload['content_type']; 
                    } 
                } 
                $countReferencePhoto++;
            } 
            //now save the file name details to database 
            $count= count($image_file_name); 
            for($x=0; $x<$count; $x++){
                $data3['property_id']= $property->id ;
                $data3['image_file_name']=     isset($image_file_name[$x]) ? $image_file_name[$x] : '';
                $data3['image_file_size']=     isset($image_file_size[$x]) ? $image_file_size[$x] : 0;
                $data3['image_content_type'] = isset($image_content_type[$x]) ? $image_content_type[$x] : '';
                $this->propertyRepo->storeImageDetails($data3);
            }
        } 

        return redirect()->route('property.index')->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){ 
        $posts= $this->propertyRepo->getById($id,"user");
        
        return view('Property.views.edit', compact('posts'));
    }
    public function update(PropertyRequest $request, $id)
    {
        $property= $this->propertyRepo->update($request->all(),$id);  
        //add the secondary images
        //let users upload maximum of 3 photos 
        if($request->hasFile('icon')){ 
            $countReferencePhoto=0; 
            $filepath= $this->filepath.$id.'/'; 

            //now delete previous files if exists in the directory
            $deleted= File::cleanDirectory(public_path().$filepath); 
            //delete all images from database
            $this->propertyRepo->destroyImageDetails($id);
            //upload the files and get its image details
            //save it in database
            $image_file_name= array();
            $image_file_size= array();
            $image_content_type= array(); 

            $uploadInstance= new FileUploadController();

            foreach ($request->file('icon') as $file) { 
                if(!in_array($file->getClientOriginalExtension(), ['jpg','png','jpeg'])){
                    return back()->withErrors("Invalid file uploaded. Please upload image file.");
                }
                //restrict to value of imageCountAllowed
                if($countReferencePhoto < $this->imageCountAllowed){   
                    $upload= $uploadInstance->file_upload($file, $filepath, $watermark=false, $resize=true);
                    if($upload){  
                        $image_file_name[]=  $upload['file_name'];
                        $image_file_size[]= $upload['file_size'];
                        $image_content_type[] = $upload['content_type']; 
                    } 
                } 
                $countReferencePhoto++;
            } 
            //now save the file name details to database 
            $count= count($image_file_name); 
            for($x=0; $x<$count; $x++){
                $data3['property_id']= $id ;
                $data3['image_file_name']=     isset($image_file_name[$x]) ? $image_file_name[$x] : '';
                $data3['image_file_size']=     isset($image_file_size[$x]) ? $image_file_size[$x] : 0;
                $data3['image_content_type'] = isset($image_content_type[$x]) ? $image_content_type[$x] : '';  
                $this->propertyRepo->storeImageDetails($data3);
            }
        }  

        return redirect()->route('property.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->propertyRepo->destroy($id); 
        return redirect()->route('property.index')->withFlashSuccess('It is deleted successfully.');
    } 
}