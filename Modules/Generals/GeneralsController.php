<?php

namespace Modules\Generals;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Generals\GeneralsRepository; 
use Modules\Generals\GeneralsRequest;
use Modules\Misc\FileUpload\FileUploadController;

class GeneralsController extends Controller
{
    private $generalsRepo;
    public function __construct(GeneralsRepository $generalsRepo){
        $this->generalsRepo =  $generalsRepo;
        $this->filepath= '/files/generals/';
    } 
     
    public function index()
    {
        $posts = $this->generalsRepo->getPaginate() ; 
        $iconPath = 'files/generals/';
        return view('Generals.views.index', compact('posts','iconPath'));
    }
    public function create()
    {  
        $generals = $this->generalsRepo->getFirst();  
        if($generals) {
            return redirect()->route('generals.edit',$generals->id);
        }
        return view('Generals.views.create');
    }
    public function store(GeneralsRequest $request)
    {
        //upload all images in repective places
        //and add data to database
        $favicon= $request->file('favicon');
        $logo= $request->file('logo'); 
        $watermark= $request->file('watermark'); 
        $default_image= $request->file('default_image'); 
        $filepath= $this->filepath;

        $uploadInstance= new FileUploadController(); 
        
        $upload= $uploadInstance->file_upload($favicon, $filepath);
        if($upload)
        {
            $request['favicon_file_name']=  $upload['file_name']; 
        }
        $upload= $uploadInstance->file_upload($logo, $filepath);
        if($upload)
        {
            $request['logo_file_name']=  $upload['file_name']; 
        }
        $upload= $uploadInstance->file_upload($watermark, $filepath);
        if($upload)
        {
            $request['watermark_file_name']=  $upload['file_name']; 
        }

        $upload= $uploadInstance->file_upload($default_image, $filepath);
        if($upload)
        {
            $request['default_image_name']=  $upload['file_name']; 
        }

        $generals = $this->generalsRepo->store($request->all());

         return redirect()->route('generals.edit', $generals->id)->withFlashSuccess('It has been added successfully.');
    }

    public function show($id)
    { 
    }
    public function edit($id){
        $posts= $this->generalsRepo->getById($id);
        
        return view('Generals.views.edit', compact('posts'));
    }
    public function update(GeneralsRequest $request, $id)
    {

        $uploadInstance= new FileUploadController();
        $generals = $this->generalsRepo->getById($id); 
        
        //update images if uploaded,  in repective places
        //and add data to database
        $filepath= $this->filepath;
        $array_of_files= array('favicon','logo','watermark','default_image');
        foreach ($array_of_files as $a) {
            if($a=='favicon'){
                $image_file_name= $generals->favicon_file_name;
            }
            elseif($a=='logo'){
                $image_file_name= $generals->logo_file_name;
            }
            elseif($a=='watermark'){
                $image_file_name= $generals->watermark_file_name;
            }
            elseif($a=='default_image'){
                $image_file_name= $generals->default_image_name;
            }
            else{} 
            
            if($request->hasFile($a))
            {   
                //delete previous icon 
                $file_deleted= fileDeleteIfExist($filepath,$image_file_name);
            }
        }
         
        $favicon= $request->file('favicon');
        $logo= $request->file('logo'); 
        $watermark= $request->file('watermark'); 
        $default_image= $request->file('default_image'); 
        
        $upload= $uploadInstance->file_upload($favicon, $filepath);
        if($upload)
        {
            $request['favicon_file_name']=  $upload['file_name']; 
        }
        $upload= $uploadInstance->file_upload($logo, $filepath);
        if($upload)
        {
            $request['logo_file_name']=  $upload['file_name']; 
        }
        $upload= $uploadInstance->file_upload($watermark, $filepath);
        if($upload)
        {
            $request['watermark_file_name']=  $upload['file_name']; 
        } 
        $upload= $uploadInstance->file_upload($default_image, $filepath);
        if($upload)
        {
            $request['default_image_name']=  $upload['file_name']; 
        }  

        $generals= $this->generalsRepo->update($request->all(),$id);

        return redirect()->route('generals.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    public function destroy($id)
    {
        $this->generalsRepo->destroy($id); 
        return redirect()->route('generals.index')->withFlashSuccess('It is deleted successfully.');
    } 
}