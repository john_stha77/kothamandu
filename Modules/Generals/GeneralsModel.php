<?php

namespace Modules\Generals;
use Core\Model\CoreModel; 

class GeneralsModel extends CoreModel 
{	
	public $table = "generals";
    protected $guarded = ['id','favicon','logo','watermark','default_image'];
}