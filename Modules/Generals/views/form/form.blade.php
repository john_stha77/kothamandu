<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Title of Dashboard
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('title_dashboard', null ,['class' => 'form-control'] ) !!}
    </div>
</div> 
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Title of Frontpage
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('title_frontpage', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
    Meta Description
    </label>
    <div class="col-md-6 col-sm-7 col-xs-12">
    {!! Form::text('meta_description_front', null ,['class' => 'form-control'] ) !!}
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Favicon </label>
    <div class="col-md-6 col-sm-7 col-xs-12"> 
        @if(isset($posts->favicon_file_name))
        @if($posts->favicon_file_name !=null)
            <img class="img-thumbnail" src="{{asset('files/generals/small/'.$posts->favicon_file_name)}}"  height="150" width="150">
        @endif
        @endif
        Browse {{Form::file('favicon',null, ['class'=>'form-control'])}} 
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Logo </label>
    <div class="col-md-6 col-sm-7 col-xs-12"> 
        @if(isset($posts->logo_file_name))
        @if($posts->logo_file_name !=null)
            <img class="img-thumbnail" src="{{asset('files/generals/small/'.$posts->logo_file_name)}}"  height="150" width="150">
        @endif
        @endif
        Browse {{Form::file('logo',null, ['class'=>'form-control'])}} 
         
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Watermark </label>
    <div class="col-md-6 col-sm-7 col-xs-12"> 
        @if(isset($posts->watermark_file_name))
        @if($posts->watermark_file_name !=null)
            <img class="img-thumbnail" src="{{asset('files/generals/small/'.$posts->watermark_file_name)}}"  height="150" width="150">
        @endif
        @endif
        Browse {{Form::file('watermark',null, ['class'=>'form-control'])}} 
    </div>
</div>
<div class="form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Contact Documentation"> Default Image </label>
     <div class="col-md-6 col-sm-7 col-xs-12"> 
        @if(isset($posts->default_image_name))
        @if($posts->default_image_name !=null)
            <img class="img-thumbnail" src="{{asset('files/generals/small/'.$posts->default_image_name)}}"  height="150" width="150">
        @endif
        @endif
        Browse {{Form::file('default_image',null, ['class'=>'form-control'])}} 
    </div>
</div> 