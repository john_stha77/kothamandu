<?php
namespace Modules\Generals;
use Core\Repository\CoreRepository;
use Modules\Generals\GeneralsModel;
use Modules\Generals\GeneralsViewModel;
class GeneralsRepository extends CoreRepository{
    public function __construct(GeneralsModel $generalsModel){
        parent::__construct($generalsModel);
    }
    public function getFirst(){
    	$first= GeneralsModel::first();
    	return $first;
    }
}