<?php

namespace Modules\Misc\FileUpload;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Generals\GeneralsModel;
use File; 
use Intervention\Image\ImageManagerStatic as Image;
class FileUploadController extends Controller
{
    public function __construct(){ 
        $generals= GeneralsModel::first();
        $this->watermarkName= ($generals!=null) ? $generals->watermark_file_name : "";
        $this->sizeArray=["large"=>"1024","medium"=>"420","small"=>"200","extra-small"=>"60"];
    }
    public function file_upload($file, $filepath, $watermark=false,$resize=true)
    {    
        if($file)
        {    
            //icon upload  
            $file_content_type = $file->getMimeType();
            $extension = $file->getClientOriginalExtension();
            $feat  = $file->getClientOriginalName();
            $name_only = explode(".".$extension, $feat);
            $name_only= $name_only[0]; 
            $featured= str_replace(' ', '-', $name_only);
            $featured= str_replace('.', '-', $featured); 

            $newFileName =  $featured.rand(). '.' .$extension;
            $destinationPath = public_path().$filepath;

            if(!empty($newFileName))
                {   
                    $file->move($destinationPath, $newFileName);

                    $icon = $newFileName;
                    $original_icon= $featured.'.'.$extension;
                }
       
        else{
                 $icon = '';
                 $original_icon= '';
                 return false;
                 
            } 
        if($icon != ''){
                //create new Intervention Image 
                //and resize the uploaded image
            $newFileLink= $destinationPath.$newFileName; 

            if($resize==true){  
               $eer=$this->resizeAndSave($newFileLink,$watermark);
            }    
                
            $data['file_name']=  $icon;
            $data['file_size']= $file->getClientSize(); 
            $data['content_type'] = $file_content_type;
        }
        //end icon upload

        
        return ($data);
        }
        else{
            return 0; 
            }
    }
    public function resizeAndSave($newFileLink,$watermark=false){
        
        $sizeArray=$this->sizeArray; 

        if(!is_file($newFileLink)){
            return false;
        }
        foreach($sizeArray as $key => $val){
            $value= intval($val);
            //make image instance of the new file
            $imageInstance = Image::make($newFileLink); 
             
            $width= $imageInstance->width(); 
            //donot put watermark on images below 640px 
            if($watermark==true){ 
                if($value >= 640){ 
                    $watermarkName= $this->watermarkName;
                    $watermarkPath= public_path()."/files/generals/";  
                    if(is_file($watermarkPath.$watermarkName)){ 
                        $watermark = Image::make($watermarkPath.$watermarkName)->widen($width); 
                        $imageInstance->insert($watermark, 'center'); 
                    }
                    
                } 
            }
            //if width is greater than $value resize the image
            if($width > $value){
                $imageInstance->resize($value, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $fileName= basename($newFileLink);
            $fileLinkArray= explode($fileName, $newFileLink);
            $resizedPath=($key !="") ? ($key."/") : "";  
            $destinationPath= $fileLinkArray[0].$resizedPath;
            if(!is_dir($destinationPath)){
                mkdir($destinationPath);
                chmod($destinationPath, 0777);
            }
            $imageInstance->save($destinationPath.$fileName);  
                 
        }
        return true;
    }

    
}
