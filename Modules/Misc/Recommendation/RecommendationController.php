<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\Product;
use App\Models\Review;
use DB;
use Auth;
use Carbon\Carbon as Carbon;
use Hash; 
use Session;

class RecommendationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $cache_timeout= 1*60;
    public function __construct()
    {
         if(! isset($_SESSION)){
            session_start();
         } 
    }
    public function getCategoriesOfLoggedIn(){
        //if user is logged in get the categories of
        //ordered,previously viewed and session cart products
        $category_ids= array();
        $product_ids= array();
        $user_id= Auth::user()->id;
        
        //product_ids from orders
        $orders= DB::table('orders')
                    ->join('order_items','order_items.order_id','=','orders.id')
                    ->where('orders.user_id', $user_id)
                    ->where('orders.deleted_at', null)
                    ->select('order_items.product_id')
                    ->take(10)
                    ->orderBy('orders.created_at','desc')
                    ->get(); 
        foreach ($orders as $order) {
            $product_ids[]=  $order->product_id;
        }
        //get product_ids from previously viewd
        $previouslyViewed= $this->getPreviouslyViewed($user_id);
        foreach ($previouslyViewed as $prev) {
            $product_ids[]= $prev->product_id;
        }
        //get product_ids from session cart
        if(isset($_SESSION['products'])){
            $cartProducts= $_SESSION['products'];
            foreach ($cartProducts as $product) { 
                $product_ids[]= $product['id'];
            }  
        }
        $category_ids= $this->getCategoryIdsByProuctIds($product_ids);
        
        return $category_ids;
    }
    public function getPreviouslyViewed(){
        $products= null;
        $user_id= Auth::user()->id;
        if(Auth::check()){
            $products =Cache::remember('previouslyViewed'.$user_id, 10 ,function() use($user_id){
                $products= DB::table('previously_viewed')
                            ->join('products','products.id','=','previously_viewed.product_id')
                            ->join('variants','variants.product_id','=','products.id')
                            ->where('previously_viewed.user_id',$user_id)
                            ->select('variants.*','products.*')
                            ->take(10)
                            ->get();
                return $products;
            }); 
        }
        return $products;
        
    }
    public function getCategoryIdsByProuctIds($product_ids){
        //NOW GET CATEGORY_IDS OF THOSE PRODUCT_IDS
        $category_ids= array();
        $categories = DB::table('products')
                        ->join('categories_products','categories_products.product_id','=','products.id')
                        ->whereIn('products.id',$product_ids)
                        ->select('categories_products.category_id')
                        ->get();
        foreach ($categories as $category) {
            $category_ids[]= $category->category_id;
        } 
        return $category_ids;
    }
    //user is not logged in and has products in cart get category of
    //cart products
    public function getCategoriesCart(){
        
        $category_ids= array();
        $product_ids= array();
        //get product_ids from session cart
        if(isset($_SESSION['products'])){
            $cartProducts= $_SESSION['products'];
            foreach ($cartProducts as $product) { 
                $product_ids[]= $product['id'];
            }  
        }
       $category_ids= $this->getCategoryIdsByProuctIds($product_ids);

        return $category_ids;
    }
    //return random 12 products 
    public function getRandomProducts(){
        $products = DB::table('products')
                                ->join('variants','variants.product_id','=','products.id')
                                ->select('variants.*', 'products.*')
                                ->where('products.deleted_at',null)
                                ->where('products.active', true) 
                                ->take(12)
                                ->inRandomOrder(); 
        return $products;
    }
   
    public function queryRecommendation(){
       
        $category_ids= array(); 
        $product_ids= array();
        if(Auth::check()){ 
            $category_ids= $this->getCategoriesOfLoggedIn();
        } 
        elseif (isset($_SESSION['products'])){ 
           $category_ids= $this->getCategoriesCart();
        }
        else{} 

        if(! empty($category_ids)){

            //get distinct category ids
            $category_ids= array_unique($category_ids);
            //now get related products in those categories
            //take only 10 categories 
            $category_ids= array_slice($category_ids,0,10); 

            $products = DB::table('products')
                        ->join('variants','variants.product_id','products.id')
                        ->join('categories_products','categories_products.product_id','products.id') 
                        ->where('products.deleted_at',null)
                        ->where('products.active', true) 
                        ->whereIn('categories_products.category_id',$category_ids)
                        ->select('variants.*','products.*')
                        ->distinct(); 
            
        }
        else{
            //get random products 
            $products= $this->getRandomProducts();
        }
       
        return $products;
    }

    public function getMaximumPriceRecommendation(){ 
        $session_id= session::getId();
        $maxPrice= Cache::remember('maxPriceRecommendation'.$session_id, $this->cache_timeout+3, function() {
            $products= $this->queryRecommendation();
            $maxPrice= $products->max('variants.price'); 
            return $maxPrice;
        }); 
        return $maxPrice;
    }
    public function recommendation(){ 
        //since every user has separate recommendation
        // and every user are not logged in use session_id 
        // as unique identifier
        $session_id= session::getId();
        $recommendation= Cache::remember('recommendation'.$session_id, $this->cache_timeout+5, function() {
            $products= $this->queryRecommendation();
            $products= $products->paginate(12);
            return $products;
        });   
        return ($recommendation); 
    }

}
