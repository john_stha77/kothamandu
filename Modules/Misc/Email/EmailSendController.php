<?php

namespace Modules\Misc\Email;
 
use App\Http\Controllers\Controller; 
use DB;
use Mail; 


class EmailSendController extends Controller
{
    protected static $admin_email= "stha.john12@gmail.com";
    protected static  $admin_name= "Kothamandu.com";

    public function __construct()
    {
     
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //normal reminder email to any email address 
    public static function sendReminder($user){ 
        $admin_email= $this->admin_email;
        Mail::send('Misc.Email.emails.reminder',['user' => $user], function ($message) use ($user,$admin_email) { 
        $message->to($admin_email)->subject('User Information');
       });
           
        if( count(Mail::failures()) > 0 ) {
            return false;
        }   
        return true;
    }
 
    //send email address confirmation link to users
    public static function sendConfirmation($user, $confirmation_code, $password=null){  
        Mail::send('Misc.Email.emails.confirmation',['user' => $user, 'confirmation_token' => $confirmation_code, 'password' => $password], function ($message) use ($user) { $message->to($user['email'], $user['name'])->subject('Please Confirm your Email Address'); 
        }); 
        if( count(Mail::failures()) > 0 ) {
            return false;
        }   
        return true;
    }
   
    //send password reset link to the user
    public static function sendPasswordResetLink($email, $passwordResetToken){  
        Mail::send('Misc.Email.emails.passwordReset',['email'=>$email,'passwordResetToken'=>$passwordResetToken], function ($message) use ($email) { $message->to($email)->subject('Please click the link to reset password.'); 
        }); 
        if( count(Mail::failures()) > 0 ) {
            return false;
        }   
        return true;
    }
   
}
