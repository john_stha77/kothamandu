 
 
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    @include('Misc.Email.emails.includes.style_email')
    </head>
    <body>
        <h2 class="well">Reset password</h2>

        <div class="my-color">
            <h3>Thanks for using our Website.</h3><br>
            <p>Please follow the link below to reset your password for "{{$email}}".</p> 
        
            <button class="button btn-info"><a href="{{ url('password/request/'.$email.'/'.$passwordResetToken) }}"><b>Click here to Confirm</b></button><br/>

        </div>
    </body>
</html> 