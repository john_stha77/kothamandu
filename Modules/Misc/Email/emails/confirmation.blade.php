 
 
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    @include('Misc.Email.emails.includes.style_email')
    </head>
    <body>
        <h2 class="well">Confirm Your Email Address</h2>

        <div class="my-color">
            <h3>Thanks for using our Website.</h3><br>
            @if($password != null)
                <p>Your current system generated password is {{$password}}.</p> 
            @endif
            <p>Please follow the link below to confirm your email address.</p>
        
        
            <button class="button btn-info"><a href="{{ url('register/confirm/'.$confirmation_token) }}"><b>Click here to Confirm</b></button><br/>

        </div>
    </body>
</html> 