<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ApplyProperty extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apply_property', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 

            $table->bigInteger('user_id')->nullable(); 
            $table->bigInteger('property_id')->nullable();  
            $table->bigInteger('created_by')->nullable();  
            $table->bigInteger('updated_by')->nullable();  

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apply_property');
    }

}
