<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserSignInDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_signin_details', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->bigInteger('user_id')->nullable();  
            $table->string('reset_password_token',50)->nullable()->unique();
            $table->dateTime('reset_password_sent_at')->nullable();
            $table->integer('sign_in_count')->default(0);
            $table->dateTime('current_sign_in_at')->nullable();
            $table->dateTime('last_sign_in_at')->nullable();
            $table->string('current_sign_in_ip')->nullable();
            $table->string('last_sign_in_ip')->nullable();
            $table->string('confirmation_token')->nullable();
            $table->dateTime('confirmation_expiry')->nullable();
            $table->boolean('confirmed')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_signin_details');
    }
}
