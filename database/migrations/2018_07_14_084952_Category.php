<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Category extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->string('name')->nullable();
            $table->string('slug',50)->unique('index_categories_on_slug');
            $table->text('description')->nullable();
            $table->bigInteger('parent_id')->nullable(); 
            $table->boolean('active')->nullable();
            $table->string('image_file_name')->nullable();
            $table->string('image_content_type')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->dateTime('image_updated_at')->nullable(); 

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category');
    }

}
