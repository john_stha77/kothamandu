<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Generals extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generals', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->string('title_dashboard')->nullable();
            $table->string('title_frontpage')->nullable();
            $table->string('favicon_file_name')->nullable();
            $table->string('logo_file_name')->nullable();
            $table->string('watermark_file_name')->nullable();  
            $table->string('meta_description_front')->nullable(); 
            $table->string('default_image_name')->nullable(); 
            
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('generals');
    }

}
