<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Locations extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->string('name');
            $table->text('similar_names')->nullable();
            $table->string('map_name')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();  
            $table->integer('distance')->nullable()->default(0);
            $table->integer('duration')->nullable()->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }

}
