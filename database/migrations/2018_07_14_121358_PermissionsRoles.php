<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermissionsRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions_roles', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->bigInteger('permission_id')->nullable()->index('index_permissions_roles_on_permission_id'); 
            $table->bigInteger('role_id')->nullable()->index('index_permissions_roles_on_role_id');
             
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('permissions_roles');
    }
}
