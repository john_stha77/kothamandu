<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Users extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->string('name')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('password')->default(''); 
            $table->string('remember_token')->nullable();
            $table->dateTime('remember_created_at')->nullable(); 
            $table->boolean('active')->nullable()->default(1);
            $table->string('email',50)->default('')->unique('index_users_on_email');
 
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }

}
