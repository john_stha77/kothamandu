<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Advs extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advs', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->boolean('active')->nullable();
            $table->integer('order')->nullable();
            $table->bigInteger('property_id')->nullable(); 
            $table->string('source')->nullable();
            $table->string('image_file_name')->nullable();
            $table->string('image_content_type')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->dateTime('image_updated_at')->nullable();
            $table->string('link')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advs');
    }

}
