<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Featured extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featured', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->bigInteger('property_id')->nullable(); 
            $table->boolean('active')->nullable();
            $table->integer('order')->nullable(); 

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('featured');
    }

}
