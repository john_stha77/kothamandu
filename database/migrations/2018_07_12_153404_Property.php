<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Property extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->string('name')->nullable();
            $table->string('sku',20)->nullable()->unique();
            $table->string('category_id')->nullable();
            $table->bigInteger('user_id')->nullable(); 
            $table->bigInteger('created_by')->nullable(); 
            $table->bigInteger('updated_by')->nullable(); 
            $table->string('location_id')->nullable();  
            $table->text('description')->nullable(); 
            $table->boolean('discountable')->nullable()->default(1);
            $table->integer('price')->default(0);
            $table->integer('sale_price')->default(0);
            $table->boolean('active')->default(1);
            $table->string('status')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->text('keywords')->nullable(); 
            $table->bigInteger('view_count')->nullable()->default(0);  

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property');
    }

}
