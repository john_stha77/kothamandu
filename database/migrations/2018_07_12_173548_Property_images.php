<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PropertyImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_images', function(Blueprint $table)
        {
            $table->bigInteger('property_id')->nullable(); 
            $table->string('image_file_name')->nullable();
            $table->string('image_content_type')->nullable();
            $table->integer('image_file_size')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('property_images');
    }
}
