<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Sliders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->string('name')->nullable();
            $table->string('link')->nullable(); 
            $table->text('description')->nullable();
            $table->string('image_file_name')->nullable();
            $table->string('image_content_type')->nullable();
            $table->integer('image_file_size')->nullable();
            $table->dateTime('image_updated_at')->nullable();
            $table->boolean('active')->nullable()->default(0);
            $table->integer('order')->nullable()->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sliders');
    }

}
