<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolesUsers extends Migration
{            
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_users', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->nullable()->index('index_roles_users_on_user_id'); 
            $table->bigInteger('role_id')->nullable()->index('index_roles_users_on_role_id'); 
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles_users');
    }
}
