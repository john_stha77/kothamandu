<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function(Blueprint $table)
        {
            $table->bigIncrements('id'); 
            $table->bigInteger('user_id')->nullable()->index('index_user_details_on_user_id'); 
            $table->string('phone_number_secondary')->nullable();
            $table->string('address')->nullable();
            $table->dateTime('dob')->nullable(); 
            $table->string('avatar_file_name')->nullable();
            $table->string('avatar_content_type')->nullable();
            $table->integer('avatar_file_size')->nullable();
            $table->dateTime('avatar_updated_at')->nullable(); 
            $table->integer('credit_amount')->nullable(); 

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }
}
