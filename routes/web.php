<?php
use Modules\RoutesServiceProvider;
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
//auth routes
Route::group(['namespace'=>'Modules'], function() {
	Auth::routes();  
});

//Routes file from modules folder
$mRoutes = new RoutesServiceProvider();  
Route::group(['prefix'=>'admin','namespace'=>'Modules', 'middleware'=>['auth']], function() use($mRoutes) {
	$mRoutes->runRoutes();
	
});

//route for ajax methods
Route::get('user-ajax','Modules\Users\ajax\Users@dataAjax'); 
Route::get('property-ajax','Modules\Property\ajax\Properties@dataAjax'); 
Route::get('location-ajax','Modules\Locations\ajax\Locations@dataAjax'); 
Route::get('category-ajax','Modules\Category\ajax\Category@dataAjax'); 

//password reset routes
Route::group(['prefix'=>'', 'namespace'=>'Modules\Auth'], function() {
	Route::get('password/reset', 'PasswordController@viewPasswordReset')->name('viewPasswordReset'); 
	Route::get('email/password', 'PasswordController@viewEmailField')->name('viewEmailField'); 
	Route::get('password/request/{email}/{token}', 'PasswordController@getPasswordRequest')->name('getPasswordRequest'); 
	Route::post('email/password', 'PasswordController@postEmailField')->name('postEmailField'); 
	Route::post('password/reset', 'PasswordController@postPasswordReset')->name('postPasswordReset'); 
	Route::get('password/postReset', 'PasswordController@passwordResetSuccessMessage')->name('passwordResetSuccessMessage');  
});

//frontend routes
	Route::group(['prefix'=>'', 'namespace'=>'App\Http\Controllers\Frontend'], function(){
		Route::get('/', 'HomeController@index')->name('home'); 
	});

	//static pages routes
	Route::group(['prefix'=>'pages','namespace'=>'App\Http\Controllers\Frontend'], function() {
		Route::get('about-us', 'PagesController@about'); 
		Route::get('contact-us', 'PagesController@contactUs'); 
	});
	//display property grid/list
	Route::group(['prefix'=>'','namespace'=>'App\Http\Controllers\Frontend'], function() {
		Route::get('items-grid/{page}/{category_id?}', 'ItemsDisplayController@itemsGrid')->name("items-grid"); 
		Route::get('item-details/{item_id}', 'ItemsDisplayController@itemDetails')->name("item-details");   
	});
	//property
	Route::group(['prefix'=>'property','namespace'=>'App\Http\Controllers\Frontend'], function() {
		Route::get('post', 'PropertyController@getPostProperty')->name("post-property-form");
		Route::post('post', 'PropertyController@postProperty')->name("post-property");     
		Route::get('applyProperty/{property_id}', 'PropertyController@applyProperty')->name("apply-property"); 
		Route::get('removeAppliedProperty/{property_id}', 'PropertyController@removeAppliedProperty')->name("remove-applied-property"); 
	});
	//property
	Route::group(['prefix'=>'account','namespace'=>'App\Http\Controllers\Frontend'], function() {
		Route::get('change-password', 'AccountController@changePasswordForm')->name("account.changePasswordForm");     
		Route::post('change-password', 'AccountController@changePassword')->name("account.changePassword");     
		Route::get('profile', 'AccountController@profile')->name("account.profile"); 
		Route::post('profile', 'AccountController@updateProfile')->name("account.updateProfile");  
		//enquires by customer
		Route::get('send-enquiry', 'AccountController@sendEnquiry')->name("send.enquiry"); 
	});
	


	//facebook sociallite
	Route::get('login/facebook', 'Modules\Auth\LoginController@redirectToProvider')->name('login/facebook');
	Route::get('login/facebook/callback','Modules\Auth\LoginController@handleProviderCallback');