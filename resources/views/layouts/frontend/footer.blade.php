 
<div class="footer">

  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-sm-3">
       <h4>Information</h4>
       <ul class="row">
        <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{url('about-us')}}">About</a></li> 
        <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{url('contact-us')}}">Contact</a></li>
      </ul>
    </div>
    
    <div class="col-lg-3 col-sm-3">
      <h4>Newsletter</h4>
      <p>Get notified about the latest properties in our marketplace.</p>
      <form class="form-inline" role="form">
        <input type="text" placeholder="Enter Your email address" class="form-control">
        <button class="btn btn-success" type="button">Notify Me!</button>
      </form>
    </div>
      
    <div class="col-lg-3 col-sm-3">
      <h4>Follow us</h4>
      <a href="#"><img src="{{asset('frontend/images/facebook.png')}}" alt="facebook"></a>
      <a href="#"><img src="{{asset('frontend/images/twitter.png')}}" alt="twitter')}}"></a>
      <a href="#"><img src="{{asset('frontend/images/linkedin.png')}}" alt="linkedin')}}"></a>
      <a href="#"><img src="{{asset('frontend/images/instagram.png')}}" alt="instagram')}}"></a>
    </div>

    <div class="col-lg-3 col-sm-3">
      <h4>Contact us</h4>
      <p><b>kothamandu.com</b><br>
        <span class="glyphicon glyphicon-map-marker"></span>Baneshowr,Kathmandu <br>
        <span class="glyphicon glyphicon-envelope"></span> info@kothamandu.com<br>
        <span class="glyphicon glyphicon-earphone"></span> 00977-</p>
      </div>
    </div>
    <p class="copyright">Copyright 2018. All rights reserved.	</p>


  </div>
</div>





    
 


