
		<div class="container">
			<ul id="gn-menu" class="gn-menu-main">
				<li class="gn-trigger">
					<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
					<nav class="gn-menu-wrapper">
						<div class="gn-scroller">
							<ul class="gn-menu">
								<li class="gn-search-item">
									{{Form::open(['url'=>'items-grid/search', 'method' => 'GET', 'data-parsley-validate'])}}
										<input name="search" placeholder="Search" type="search" class="gn-search">
										<a class="gn-icon gn-icon-search"><span>Search</span></a>
									{{Form::close()}}
								</li>
								@if(Auth::check())
								<li><a href="{{url('account/change-password')}}" class="gn-icon gn-icon-cog">Change password</a></li>
								<li><a href="{{url('items-grid/postedProperty')}}" class="gn-icon gn-icon-download">Posted property</a></li>
								<li><a href="{{url('items-grid/appliedProperty')}}" class="gn-icon gn-icon-download">Applied property</a></li>
								<li><a href="{{url('account/profile')}}" class="gn-icon gn-icon-download">Be an agent</a></li>
								<li><a href="{{url('account/profile')}}" class="gn-icon gn-icon-download">Profile</a></li> 
								@endif
							</ul>
						</div><!-- /gn-scroller -->
					</nav>
				</li>
	
			</ul>
		
		</div><!-- /container -->
	 