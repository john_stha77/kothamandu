<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="{{$generals['meta_description_front']}}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$generals['title_frontpage']}}</title>
    <link rel="shortcut icon" href="{{asset('files/generals/'.$generals['favicon_file_name'])}}"  type="image/x-icon">


    @include('partials.styles_front')
  </head>
  
  <body>
      
      @include('layouts.frontend.header')
      @include('layouts.frontend.sidebar')
      @yield('content')
      @include('layouts.frontend.footer')
     
  </body>
    @include('partials.scripts_front')
</html> 
