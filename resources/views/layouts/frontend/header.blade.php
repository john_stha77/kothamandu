
    <!-- container starts -->
    <div class="container">
      <?php $url= route('register');?>
    <!-- Header Starts -->
    <div class="header">
      <a href="{{url('/')}}"><img src="{{asset('files/generals/'.$generals['logo_file_name'])}}" alt="Realestate"></a>

      <ul class="pull-right">
        <li><a href="{{route('post-property')}}">Post Property</a></li>
        @if(!Auth::check()) 
        <li><!-- <button class="btn btn-info"   data-toggle="modal" data-target="#loginpop">Login</button>  -->
            <button type="submit" class="btn btn-info"  onclick="window.location.href='{{$url}}'">Log in</button>
            <button type="submit" class="btn btn-info"  onclick="window.location.href='{{$url}}'">Join Now</button>
        </li> 
        @else
        
        <li><a href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out pull-right"></i> Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li> 
        @endif
        
      </ul> 

    </div>
    <!-- #Header Starts -->
  
<!-- Modal -->
   <!--  <div id="loginpop" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="row">
            <div class="col-sm-6 login">
              <h4>Login</h4>
              <form class="form-horizontal" method="POST" action="{{ route('login') }}" role="form">
                {{ csrf_field() }}

                <div class="form-group">
                  <label class="sr-only" for="exampleInputEmail2">Email address</label>
                  <input type="email" name="email" value="{{ old('email') }}" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <label class="sr-only" for="exampleInputPassword2">Password</label>
                  <input type="password" name="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" checked="{{ old('remember') ? 'checked' : '' }}"> Remember me
                  </label>
                </div>
                <button type="submit" class="btn btn-success">Sign in</button>
              </form>          
            </div>
            <div class="col-sm-6">
              <h4>New User Sign Up</h4>
              <p>Join today and get updated with all the properties deal happening around.</p>
              <?php $url= route('register');?>
              <button type="submit" class="btn btn-info"  onclick="window.location.href='{{$url}}'">Join Now</button>
              <a class="btn btn-link" href="{{ route('viewEmailField') }}">Forgot Your Password?</a>
            </div>

          </div>
        </div>
      </div>
    </div> -->
    <!-- /.modal -->
</div>