<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{$generals['title_dashboard']}}</title>
    <link rel="shortcut icon" href="{{asset('files/generals/'.$generals['favicon_file_name'])}}" type="image/x-icon">
    
    @include('partials.styles')
  </head>
  
 <body class="@yield('class-body', 'nav-md' )">
    <div class="container body">
      <div class="main_container">
       
    @include('layouts.backend.sidebar')
    @include('layouts.backend.header')
    
    <div class="right_col" role="main">
      <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
              @yield('content')
          </div>
      </div>
    </div>

    @include('layouts.backend.footer')
    
     </div>
    </div>
    <!-- /#wrapper --> 
    @include('partials.scripts')
</body>

</html> 
