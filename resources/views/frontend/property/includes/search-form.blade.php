<div class="search-form"><h4><span class="glyphicon glyphicon-search"></span> Search for</h4>
  {{Form::open(['url'=>'items-grid/search', 'method' => 'GET', 'data-parsley-validate'])}}
    <input type="text" class="form-control" name="search" placeholder="Search of Properties">
    <div class="row">
        <!-- <div class="col-lg-5">
          <select class="form-control">
            <option>Buy</option>
            <option>Rent</option>
            <option>Sale</option>
          </select>
        </div> -->
        <div class="col-lg-6">
          <input class="form-control" name="price_range" placeholder="Price1-Price2">
        </div>
        <div class="col-lg-6">
          <input class="form-control" name="location" placeholder="Location">
        </div>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <select class="form-control" name="category_id">
                <option>-- Property Type --</option>
                @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option> 
                @endforeach
              </select>
        </div>
      </div>
      <button class="btn btn-primary">Find Now</button>
      {{Form::close()}}
  </div>