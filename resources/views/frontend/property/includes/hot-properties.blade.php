<div class="hot-properties hidden-xs">
<h4>Hot Properties</h4>
@foreach($hotProperties as $property) 
  <div class="row">  
    <div class="col-lg-4 col-sm-5"><img src="{{asset('files/property/'.$property->id.'/small/'.(isset($property->image_file_name) ? $property->image_file_name : ''))}}" class="img-responsive img-circle" alt="{{$property->description}}"></div>
    <div class="col-lg-8 col-sm-7">
      <h5><a href="{{url('item-details/'.$property->id)}}">{{$property->name}}</a></h5>
      <p class="price">Rs. {{$property->name}}</p> </div>
  </div>
@endforeach
 

</div>