<?php $current_url= url()->current(); ?> 
<div class="sortby clearfix">
<div class="pull-left result"></div>
  <div class="pull-right">
  <select class="form-control" onchange="location = this.value;"">
  <option>Sort by</option>
  <option value="{{$current_url.'?sort=new'}}">Newest</option>
  <option value="{{$current_url.'?sort='.(isset($_SESSION['sortPrice']) ? $_SESSION['sortPrice'] : 'price_desc')}}">By Price </option>
  <option value="{{$current_url.'?sort='.(isset($_SESSION['sortName']) ? $_SESSION['sortName'] : 'name_asc')}}">Name</option>
   
</select></div>

</div>