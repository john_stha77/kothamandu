@extends('layouts.frontend.master')
@section('content')
 
@include("frontend.property.includes.banner") 

<div class="container">
<div class="properties-listing spacer"> 
	@include('error-success')
<div class="row">
  <div class="col-lg-3 col-sm-4 ">

    @include("frontend.property.includes.search-form") 
  </div>
  
  <div class="col-lg-9 col-sm-8">
  	{{Form::open(['route'=>'post-property','data-parsley-validate', 'files'=>'true'])}}
  		<input type="text" name="name" class="form-control" placeholder="Property Name">
      <input type="text" name="owner_name" class="form-control" placeholder="Owner's Name">
  		<select class="category_search form-control" name="category_id"></select><br><br>
  		<select class="location_search form-control" name="location_id"></select><br><br>
  		<input type="file" name='icon[]' class="form-control" multiple>  
      <input type="text" name="price" class="form-control" placeholder="Price">
      <select name="discountable" class="form-control"> 
        <option>-- Select --</option>
      	<option value="1">discountable</option>
      	<option value="0">not discountable</option>
      </select> 
      <textarea  name="description" rows="6" class="form-control" placeholder="Message"></textarea>
    	<button type="submit" class="btn btn-success">Post Property</button>
    {{Form::close()}}
  </div>
</div>
</div>
</div>

<script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js')}}"></script>
@endsection  	