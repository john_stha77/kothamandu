@extends('layouts.frontend.master')
@section('content')
 
@include("frontend.property.includes.banner") 

<div class="container">
<div class="properties-listing spacer"> 
<div class="row">
  <div class="col-lg-3 col-sm-4 ">

    @include("frontend.property.includes.search-form")
    @include("frontend.property.includes.hot-properties")  
  </div>
  
  <div class="col-lg-9 col-sm-8">
    @if($items!=null)
    <?php $itemsCount= $items->count(); ?>
    @include("frontend.property.includes.top-nav") 
      <div class="row">

        @if($itemsCount > 0)
          @foreach($items as $property) 
           <!-- properties -->
            <div class="col-lg-4 col-sm-6">
            <div class="properties">
              <div class="image-holder"><img src="{{asset('files/property/'.$property->id.'/small/'.(isset($property->image_file_name) ? $property->image_file_name : ''))}}" width="198" height="128"  alt="properties">
                <div class="status sold">{{$property->status}}</div>
              </div>
              <h4><a href="{{url('item-details/'.$property->id)}}">{{$property->name}}</a></h4>
              <p class="price">Rs. {{$property->price}}</p>
              <!-- <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div> -->
              <a class="btn btn-primary" href="{{url('item-details/'.$property->id)}}">View Details</a>
            </div>
            </div>
            <!-- properties -->
          @endforeach
            <div class="center">
            <!-- <ul class="pagination">
              <li><a href="#">«</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#">»</a></li>
            </ul> -->
          {!!$items->links()!!}
      </div>
      @else
        <p>Sorry, donot match any property listed.</p>
      @endif

    </div> 
  @endif
</div>
</div>
</div>
</div>
@endsection