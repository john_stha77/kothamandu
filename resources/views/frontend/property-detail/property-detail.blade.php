@extends('layouts.frontend.master')
@section('content')
<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <span class="pull-right"><a href="#">Home</a> / Buy</span>
    <h2>Buy</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="properties-listing spacer">

<div class="row">
<div class="col-lg-3 col-sm-4 hidden-xs">
 
@include("frontend.property.includes.hot-properties") 
@include("frontend.property-detail.includes.advertisements")  

</div>

<div class="col-lg-9 col-sm-8 ">

<!-- <h2>2 room and 1 kitchen apartment</h2> -->
<div class="row">
  <div class="col-lg-8">

  <!-- blog detail --> 
  <h2>{{$itemDetails->name}} ({{$itemDetails->category_name}})</h2>
  <div class="info">Posted on: {{$itemDetails->created_at}}</div>
  <img src="{{asset('files/property/'.$itemDetails->id.'/medium/'.$itemDetails->image_file_name)}}" class="thumbnail img-responsive"  alt="blog title">
  @if(!in_array($itemDetails->property_id, $applied_property_ids))
    <a href="{{url('property/applyProperty/'.$itemDetails->property_id)}}" class="btn btn-primary" style="width:200px;">Apply</a>
  @else
    <a href="{{url('property/removeAppliedProperty/'.$itemDetails->property_id)}}" class="btn btn-primary" style="width:200px;">Undo Apply</a>
  @endif
  <p>Location: {{$itemDetails->location_name}} </p>
  <p>Price: {{$itemDetails->price}} {{($itemDetails->discountable!=true) ? "Negotiable" : ""}}</p>
  {!!$itemDetails->description!!}





  </div>
  @include("frontend.property-detail.includes.right-side-bar") 
</div>
</div>
</div>
</div>
</div>

@endsection