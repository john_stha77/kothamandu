  <div class="col-lg-4">
  <div class="col-lg-12  col-sm-6">
<div class="property-info">
<p class="price">{{$itemDetails->price}}</p>
  <p class="area"><span class="glyphicon glyphicon-map-marker"></span> {{$itemDetails->location_name}}</p>
  
  @foreach($agents as $a)
  <div class="profile">
  <span class="glyphicon glyphicon-user"></span> Agent Details
  <p>{{$a->user->name}}<br>{{$a->user->phone_number}}</p>
  </div>
  @endforeach
</div>

    <h6><span class="glyphicon glyphicon-home"></span> Status</h6>
    <div class="listing-detail">
    <!-- <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> --> 
    {{$itemDetails->status}}
    </div>

</div>
@include("frontend.property-detail.includes.post-enquiry") 
</div>