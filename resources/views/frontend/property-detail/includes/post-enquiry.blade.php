<div class="col-lg-12 col-sm-6 ">
<div class="enquiry">
  <h6><span class="glyphicon glyphicon-envelope"></span> Post Enquiry</h6>
  <form action="{{route('send.enquiry')}}" method="GET">
    <input type="text" name="name" class="form-control" placeholder="Full Name"/>
    <input type="text" name="email" class="form-control" placeholder="you@yourdomain.com"/>
    <input type="text" name="phone_number" class="form-control" placeholder="your number"/>
    <textarea rows="6" name="message" class="form-control" placeholder="Whats on your mind?"></textarea>
	<button type="submit" class="btn btn-primary" >Send Message</button>
  </form>
 </div>         
</div>