
  @extends('layouts.frontend.master')
  @section('content')
  <!-- banner -->
  <div class="inside-banner">
    <div class="container"> 
      <span class="pull-right"><a href="#">Home</a> / Register</span>
      <h2>Register</h2>
    </div>
  </div>
  <!-- banner -->
  <div class="container">
    <h2>Register/Login</h2>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#login">Login</a></li>
      <li><a data-toggle="tab" href="#register">Register</a></li>
    </ul>

    <div class="tab-content">
      <div id="login" class="tab-pane fade in active">
        <h3>LOGIN</h3>
        <p><div class="row login">
          <div class="col-md-6">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}" role="form">
              {{ csrf_field() }}
              <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Enter email">
              <input type="password" name="password" class="form-control" placeholder="Password">
              <input type="checkbox" checked="{{ old('remember') ? 'checked' : '' }}"> Remember me
              <button type="submit" class="btn btn-success">Sign in</button>
            </form>
            <a class="btn btn-link" href="{{ route('viewEmailField') }}">Forgot Your Password?</a> 
            <a href="{{route('login/facebook')}}"  class="btn btn-primary" style="background-color: #21248c;">Login with facebook</a>
             
          </div>
        </div>
      </p>
    </div>
    <div id="register" class="tab-pane fade">
      <h3>REGISTER</h3>
      <p>
        <div class="row register">
          <div class="col-md-6>
            <form class="form-horizontal" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }} 
              <input type="text" class="form-control" placeholder="Full Name" name="name">
              <input type="email" class="form-control" placeholder="Enter Email" name="email">
              <input type="password" class="form-control" placeholder="Password" name="password">
              <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation"> 
              <button type="submit" class="btn btn-success">Register</button>
            </form>


            
          </div>
          
        </div>
      </p>
    </div>
  </div>
</div>

@endsection