@extends('layouts.frontend.master')
@section('content')
<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <span class="pull-right"><a href="#">Home</a> / Applied Properties</span>
    <h2>Applied Properties</h2>
  </div>
</div>
<!-- banner -->


<div class="container">
  <div class="spacer agents">

    <div class="row">
      @if($items!=null)
      <div class="col-lg-8  col-lg-offset-2 col-sm-12">
        @foreach($items as $property)
        <!-- agents -->
        <div class="row">
          <div class="col-lg-2 col-sm-2 "><a href="#"><img src="{{asset('files/property/'.$property->id.'/small/'.(isset($property->property_images[0]) ? $property->property_images[0]->image_file_name : ''))}}" class="img-responsive"  alt="agent name"></a></div>
          <div class="col-lg-7 col-sm-7 "><h4>{{$property->name}}</h4>{!! $property->description !!}</div>
          <div class="col-lg-3 col-sm-3 "><span class="glyphicon glyphicon-envelope"></span> <a href="{{url('item-details/'.$property->id')}}">View details</a><br> 
          </div>
        </div>
          <!-- agents -->
        @endforeach  
        
      </div>
      @endif  
    </div>


  </div>
</div>
@endsection