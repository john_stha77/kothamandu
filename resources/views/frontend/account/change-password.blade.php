@extends('layouts.frontend.master')
@section('content')
<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <span class="pull-right"><a href="#">Home</a> / Contact Us</span>
    <h2>Contact Us</h2>
</div>
</div>
<!-- banner -->


<div class="container">
<div class="spacer">
<div class="row contact">
  @include('error-success')
  <div class="col-lg-6 col-sm-6 ">

      <form method="POST" action="{{ url('account/change-password') }}">
        {{ csrf_field() }}
        <input type="text" name="current_password" class="form-control" placeholder="Old Password">
        <input type="text" name="new_password" class="form-control" placeholder="New Password">
        <input type="text" name="confirm_password" class="form-control" placeholder="Confirm Password">
             
      <button type="submit" class="btn btn-success" name="Submit">Change Password</button>
      </form>       
  </div> 
   
</div>
</div>
</div>

@endsection