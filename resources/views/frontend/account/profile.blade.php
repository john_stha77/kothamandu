@extends('layouts.frontend.master')
@section('content')
 
<div class="container">
	<div class="spacer">
		<div class="row register">
			<form class="form-horizontal profile_form" method="POST" action="{{url('account/profile')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="col-sm-4">
				<img src="{{asset('files/'.(isset($user->user_details) ? 'users/'.$user->user_details->avatar_file_name : 'generals/'.$generals['default_image_name']))}}" height="200" width="250">
				<br/>
				<br/>
				<div class="form-group">
						<div>
							<input name="icon" type="file" class="form-control" id="email" placeholder="">
						</div>
					</div>
			</div>
			<div class="col-sm-8">
			
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Name:</label>
						<div class="col-sm-10">
							<input type="text" name="name" class="form-control" id="email" value="{{$user->name}}" placeholder="Bhawana Uprety">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Email:</label>
						<div class="col-sm-10">
							<input type="email" name="email" class="form-control" id="email" value="{{$user->email}}" placeholder="johadai@gmail.com">
						</div>
					</div>
					 
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Address:</label>
						<div class="col-sm-10">
							<input type="text" name="address" class="form-control" id="email" value="{{isset($user->user_details) ? $user->user_details->address : ''}}" placeholder="Kathmandu,Nepal">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Phone Number Secondary:</label>
						<div class="col-sm-10">

							<input type="text" name="phone_number_secondary" class="form-control" id="email" value="{{isset($user->user_details) ? $user->user_details->phone_number_secondary : ''}}" placeholder="9876543210">
						</div>
					</div> 
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Date of Birth:</label>
						<div class="col-sm-10">
							<input type="date" name="dob" class="form-control" id="email" value="{{isset($user->user_details) ? $user->user_details->dob : ''}}" placeholder="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-2" for="email">Be an Agent:</label>
						<div class="col-sm-10">
							 <select name="is_agent" class="form-control make_agent">
							 	<option>-- Select --</option>
							 	<option value="1" {{($is_agent==true) ? "selected" : ""}}>Yes</option>
							 	<option value="0" {{($is_agent!=true) ? "selected" : ""}}>No</option>
							 </select>
						</div>
					</div>
					<div class="form-group {{($is_agent!=true) ? 'hide' : ''}} expertise_location">
						<label class="control-label col-sm-2" for="email">Expertise Location:</label>
						<div class="col-sm-10">
							<select class="location_search form-control" name="expertise_location_id">
			                    @if(isset($agent->expertise_location_id))
			                    <option value="{{$agent->expertise_location_id}}">{{$agent->location->name}}</option>
			                    @endif
                			</select>
						</div>
					</div>

					<button type="submit" class="btn btn-success">Save</button>
				</form>


			</div>
			

		</div>
	</div>
</div>
@endsection