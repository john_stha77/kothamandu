@extends('layouts.frontend.master')
@section('content')




@include('frontend.home.includes.sliders')
@include('frontend.home.includes.banner-search')

<div class="container"> 
  @include('frontend.home.includes.featured')
  @include('frontend.home.includes.new-properties')
 
   
</div>
@endsection