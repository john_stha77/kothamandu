@if($featured !=null)

<div class="properties-listing spacer"> <a href="{{url('items-grid/featured')}}" class="pull-right viewall">View All Listing</a>
    <h2>Featured Properties</h2>
    <div id="owl-example" class="owl-carousel">  
      @foreach($featured as $product)  
      <div class="properties">
        <div class="image-holder"><img src="{{asset('files/property/'.$product->property_id.'/small/'.(isset($product->property_images[0]) ? $product->property_images[0]->image_file_name : ''))}}" width="198" height="128"  alt="properties"/>
          <div class="status sold">{{$product->property->status}}</div>
        </div>
        <h4><a href="{{url('item-details/'.$product->property_id)}}">{{$product->property->name}}</a></h4>
        <p class="price">Price: {{$product->property->price}}</p>
        <!-- <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room"></span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div> -->
        <a class="btn btn-primary" href="{{url('item-details/'.$product->property_id)}}">View Details</a>
      </div> 
      @endforeach
    </div>
  </div>

@endif