@if($sliders!=null)
<div class="">
    
  <!-- slider starts -->
  <div id="slider" class="sl-slider-wrapper">

    <div class="sl-slider">
    @foreach($sliders as $slider)
     
      <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
        <div class="sl-slide-inner">
          <?php $image= asset('files/sliders/'.$slider->image_file_name); ?>
          <div class="bg-img" style="background-image:url('{{$image}}');width:100%;height:100%;"></div>
          <h2><a href="{{$slider->link}}">{{$slider->name}}</a></h2>
          <blockquote>              
          <p class="location"><span class="glyphicon glyphicon-map-marker"></span> {!! $slider->description !!}
          </blockquote>
        </div>
      </div>
    @endforeach 
       
    </div>
    <!-- sl-slider  -->

    <?php $count= $sliders->count(); ?>

    <nav id="nav-dots" class="nav-dots">
      @for($i = 0; $i < $count; $i++)
        <span {{(($i==0) ? 'class="nav-dot-current"' : "")}}></span>
      @endfor
    </nav>

  </div>  
</div> 
@endif