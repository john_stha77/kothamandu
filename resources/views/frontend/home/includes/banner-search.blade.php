<div class="banner-search">
  <div class="container"> 
<!-- banner -->
    <h3>Buy, Sale & Rent</h3>
    <div class="searchbar">
      <div class="row">
        <div class="col-lg-6 col-sm-6">
          {{Form::open(['url'=>'items-grid/search', 'method' => 'GET', 'data-parsley-validate'])}}
          <input type="text" class="form-control" name="search" placeholder="Search for Properties">
          <div class="row">
            <div class="col-lg-3 col-sm-3 ">
              <select class="form-control" name="category_id">
                @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option> 
                @endforeach
              </select>
            </div>
            <div class="col-lg-3 col-sm-4">
              <!-- <select class="form-control">
                <option>Price</option>
                <option>$150,000 - $200,000</option>
                <option>$200,000 - $250,000</option>
                <option>$250,000 - $300,000</option>
                <option>$300,000 - above</option>
              </select> -->
              <input class="form-control" name="price_range" placeholder="Price1-Price2">
            </div>
            <div class="col-lg-3 col-sm-4">
              <!-- <select class="form-control">
                <option>Property</option>
                <option>Apartment</option>
                <option>Building</option>
                <option>Office Space</option>
              </select> -->
              <input class="form-control" name="location" placeholder="Location">
            </div>
            <div class="col-lg-3 col-sm-4">
              <button class="btn btn-success">Find Now</button>
            </div>
          </div>
          {{Form::close()}}
          
        </div>
        
      </div>
    </div>
  </div>
</div>
<!-- banner -->

