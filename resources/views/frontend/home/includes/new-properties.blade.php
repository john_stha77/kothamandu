@if($newProperties !=null)
 <div class="properties-listing spacer"> <a href="{{url('items-grid/hotProperties')}}" class="pull-right viewall">View All Listing</a>
    <h2>New Properties</h2>
    <div id="owl-example" class="owl-one">
      @foreach($newProperties as $product)
      <div class="properties">
          <div class="image-holder"><img src="{{asset('files/property/'.$product->id.'/small/'.(isset($product->property_images[0]) ? $product->property_images[0]->image_file_name : ''))}}"   alt="properties" width="198" height="128" />
          <div class="status sold">{{$product->status}}</div>
        </div>
        <h4><a href="{{url('item-details/'.$product->id)}}">{{$product->name}}</a></h4>
        <p class="price">Price: {{$product->price}}</p>
       <!--  <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room"></span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div> -->
        <a class="btn btn-primary" href="{{url('item-details/'.$product->id)}}">View Details</a>
      </div>
      @endforeach
    </div>
  </div> 
@endif