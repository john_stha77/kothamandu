<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/normalize.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/demo.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/component.css')}}" />

<link rel="stylesheet" href="{{asset('frontend/assets/bootstrap/css/bootstrap.css')}}" />
<link rel="stylesheet" href="{{asset('frontend/assets/style.css')}}"/>

<!-- Owl stylesheet -->
<link rel="stylesheet" href="{{asset('frontend/assets/owl-carousel/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('frontend/assets/owl-carousel/owl.theme.css')}}">
<!-- Owl stylesheet -->


<!-- slitslider -->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/slitslider/css/style.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('frontend/assets/slitslider/css/custom.css')}}" />
<!-- slitslider -->

<!-- select2 css -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

     