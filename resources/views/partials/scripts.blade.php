<!-- jQuery -->
    <script src="{{asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script> 
    
     <script src="{{asset('vendors/moment/min/moment.min.js')}}"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('build/js/custom.min.js')}}"></script> 

    <!-- Custom made Scripts -->
    <script src="{{asset('js/custom.js')}}"></script> 
    <script src="{{asset('js/custom-ajax.js')}}"></script> 
    <script src="{{asset('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js')}}"></script> 

    <script type="text/javascript">
        var root_url= "{{URL::to('/')}}/"; 
    </script>