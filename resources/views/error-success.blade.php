@if (count($errors)) 
<div class="alert alert-danger">
    <ul> 
        @foreach($errors->all() as $error) 
            <li> {!! $error !!}</li> 
        @endforeach 
    </ul> 
</div>
@endif

 @if(Session::get('flash_success'))
    <div class="alert alert-success">
        {!! Session::get('flash_success') !!}
    </div> 
 @endif