<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use Illuminate\Support\Facades\Request;
class RolesPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //simple  middleware demo
       $permissions =  DB::table('users')
                ->select('permissions.code')
                ->join('roles_users','roles_users.user_id','=','users.id')
                ->join('roles','roles.id','=','roles_users.role_id')
                ->join('permissions_roles','permissions_roles.role_id','=','roles.id')
                ->join('permissions','permissions.id','=','permissions_roles.permission_id') 
                ->where('users.id','=',Auth::user()->id)
                ->where(function($query){
                    $query->where('permissions.code','!=',null)->orWhere('permissions.code','!=','');
                })
                ->orderBy('users.id')
                ->get();  
        $allowed_routes = [];
        foreach($permissions as $permission){
            $allowed_routes[] = $permission->code;
        } 
        $routeName = Request::route()->getName() ;
        if(in_array($routeName,$allowed_routes))         
                return $next($request);
        else{
            return redirect('/');
            //throw new Exception("NoAccessExcepion", 1);
            
        }
    }
}
