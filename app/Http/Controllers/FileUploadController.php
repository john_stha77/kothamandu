<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Generals;  
use File; 
use Intervention\Image\ImageManagerStatic as Image;
class FileUploadController extends Controller
{
    protected $vari= "ok";
    
     public function file_upload($file, $filepath, $watermark=false,$resize=true)
    {   
        //donot use watermark now
        $watermark=false;
        
         if($file)
        {    
            //icon upload  
            $file_content_type = $file->getMimeType();
            $extension = $file->getClientOriginalExtension();
            $feat  = $file->getClientOriginalName();
            $name_only = explode(".".$extension, $feat);
            $name_only= $name_only[0]; 
            $featured= str_replace(' ', '-', $name_only);
            $featured= str_replace('.', '-', $featured); 

            $newFileName =  $featured.rand(). '.' .$extension;
            $destinationPath = public_path().$filepath;

            if(!empty($newFileName))
                {   
                    $file->move($destinationPath, $newFileName);

                    $icon = $newFileName;
                    $original_icon= $featured.'.'.$extension;
                }
       
        else{
                 $icon = '';
                 $original_icon= '';
                 return false;
                 
            } 
        if($icon != ''){
                //create new Intervention Image 
                //and resize the uploaded image
            $newFileLink= $destinationPath.$newFileName;
            $img = Image::make($newFileLink);  
            $sizeArray=["medium"=>"420","small"=>"140","extra-small"=>"60"];

            if($resize==true){  
               $eer=$this->resizeAndSave($img, $sizeArray,$newFileLink,$watermark);
            }    
                
            $data['file_name']=  $icon;
            $data['file_size']= $file->getClientSize(); 
            $data['content_type'] = $file_content_type;
        }
        //end icon upload

        
        return ($data);
        }
        else{
            return 0; 
            }
    }
    protected function resizeAndSave($imageInstance, $sizeArray,$newFileLink,$watermark=false){
        $width= $imageInstance->width();
        foreach($sizeArray as $key => $val){
            $value= intval($val);
            //if width is greater than $value resize the image
            //donot put watermark on images below 640px
            if($width > $value){
                if($watermark==true){ 
                    if($value >= 640){
                        $watermarkName= Generals::first()->watermark_file_name;
                        $watermarkPath= public_path()."/files/generals/"; 
                        $watermark = Image::make($watermarkPath.$watermarkName);
                        $imageInstance->insert($watermark, 'center'); 
                    } 
                }
                $imageInstance->resize($value, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $fileName= basename($newFileLink);
            $fileLinkArray= explode($fileName, $newFileLink);
            $resizedPath=($key !="") ? ($key."/") : "";  
            $destinationPath= $fileLinkArray[0].$resizedPath;
            if(!is_dir($destinationPath)){
                mkdir($destinationPath);
            }
            $imageInstance->save($destinationPath.$fileName);  
                 
        }
        return true;
    }

    
}
