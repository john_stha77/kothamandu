<?php


namespace App\Http\Controllers\Misc;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;


class Select2_search_userController extends Controller
{
    /**
     * Show the application layout.
     *
     * @return \Illuminate\Http\Response
     */
    public function layout()
    {
    	return view('backend.order.create');
    }


    /**
     * Show the application dataAjax.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataAjax(Request $request)
    {
    	$data = [];


        if($request->has('q')){
            $search = $request->q;

            $data = DB::table("users")
            		->select("id","name","phone_number","email")
                    ->where('deleted_at',null)
            		->where('name','like',"%$search%")
                    ->orWhere('phone_number','like',"%$search%") 
                    ->orWhere('email','like',"%$search%")                     
                    ->get();
                   
            
            }

        return response()->json($data);
   
    }

}