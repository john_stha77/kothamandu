<?php

namespace App\Http\Controllers\Frontend;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Property\PropertyController as BackendPropertyController; 
use Modules\Property\PropertyRequest; 
use Auth;
use Modules\ApplyProperty\ApplyPropertyRepository; 
 
class PropertyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BackendPropertyController $propertyController, ApplyPropertyRepository $applypropertyRepo)
    {
         $this->propertyController= $propertyController;
         $this->applypropertyRepo =  $applypropertyRepo;
    }
    public function getPostProperty(){
        if(!Auth::check()){
            return redirect()->route('register')->withErrors("Please login or register before posting property.");
        }
        return view("frontend.property.post-property");
    }
    public function postProperty(PropertyRequest $request){
        $user_id= Auth::user()->id;
        $request['active']= true;
        $request['user_id']= $user_id;
        $request['created_by']= $user_id; 
    	$this->propertyController->store($request);

        //posted properties link
        $postedPropertiesLink= url('items-grid/postedProperty');
        return back()->withFlashSuccess("Property added successfully. <a href='".$postedPropertiesLink."' class='btn btn-success'>View posted properties</a>");
    }
    public function applyProperty($property_id){
        $data['user_id']= Auth::user()->id;
        $data['property_id']= $property_id;
        $applyproperty = $this->applypropertyRepo->store($data);
        return back();
    }
    public function removeAppliedProperty($property_id){
        $data['user_id']= Auth::user()->id;
        $data['property_id']= $property_id;
        $applyproperty = $this->applypropertyRepo->undoApply($data);
        return back();
    }

    

    
}
	