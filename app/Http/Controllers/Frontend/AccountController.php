<?php

namespace App\Http\Controllers\Frontend;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Users\UsersRepository;
use Modules\Agents\AgentsRepository; 
use Modules\Users\UserDetailsRepository;
use Auth;
use Session;
use Hash;
use Modules\Users\UsersController;
use Modules\Users\UsersRequest;
use Modules\Enquiries\EnquiriesRepository; 
use Modules\Enquiries\EnquiriesRequest;

class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UsersRepository $usersRepository,UserDetailsRepository $userDetailsRepository,AgentsRepository $agentsRepository, UsersController $usersController, EnquiriesRepository $enquiriesRepository)
    {      
         $this->usersRepository = $usersRepository;
         $this->agentsRepository = $agentsRepository;
         $this->userDetailsRepository = $userDetailsRepository;
         $this->usersController = $usersController;  
         $this->enquiriesRepository = $enquiriesRepository;
    }
    public function profile(){ 
        $user_id= Auth::user()->id;
    	$user= $this->usersRepository->getById($user_id,['user_details','user_signin_details']);
        $is_agent= $this->agentsRepository->checkIfExists($user_id);
        $agent= $this->agentsRepository->getOneByUserId($user_id); 
    	return view('frontend.account.profile',compact('user','is_agent','agent'));
    }
    public function updateProfile(UsersRequest $request){
        $user_id= Auth::user()->id; 
        $this->usersController->update($request, $user_id); 
        if($request['is_agent']=="1"){
            $agentData['user_id']= $user_id;
            $agentData['expertise_location_id']= $request['expertise_location_id'];
            $agentDetails = ($this->agentsRepository->checkIfExists($user_id)==true) ? $this->agentsRepository->updateByUserId($agentData,$user_id) : $this->agentsRepository->store($agentData); 
        }
        else{
            $this->agentsRepository->destroyByUserId($user_id);
        }
        
        return back()->withFlashSuccess("Successfully, updated the profile.");

    }
    public function changePasswordForm(){
        //return 'change-password';
        Session::put('account_active_menu','changepassword');
        return view('frontend.account.change-password');
    }

    public function changePassword(Request $request){
        //return $request->all();
        $user_id= Auth::user()->id;
        $user= $this->usersRepository->getById($user_id);
        if (!Hash::check($request['current_password'], $user->password)){
            Session::flash('error', ' Looks like the entered current password is wrong');
            return back()->withInput();
        }
        if ($request['new_password'] != $request['confirm_password']){
            Session::flash('error', 'Confirm password do not match');
            return back();
        }
        $makePassword = Hash::make($request['new_password']);
        $this->usersRepository->update(['password'=>$makePassword],$user_id);
        Session::flash('success','Password Changed successfully') ;
        Session::put('account_active_menu','changepassword');
        return back();
    }
 
    public function credit(){
        $credit= $this->userDetailsRepository->getOneByUserId(Auth::user()->id)->credit_amount;
        return $credit;
    }
    public function history(){

    }
    public function agent(){
       $agents=  $this->agentsRepository->getPaginate(6);
       return view("frontend.agent.agents",compact('agents'));
    }
    public function postedProperty(){

    }
    public function sendEnquiry(EnquiriesRequest $request){ 
        $this->enquiriesRepository->store($request->all());
        return back();
    }
    public function appliedProperty(){

    }


    

    

    
}
