<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Support\Facades\Cache;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Auth;
use Carbon\Carbon as Carbon;
use Session; 
use Modules\Property\PropertyRepository; 
use Modules\Agents\AgentsRepository; 
use Modules\Advs\AdvsRepository; 
use Modules\ApplyProperty\ApplyPropertyRepository; 

class ItemsDisplayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $cache_timeout= 1*60;
    protected $long_cache_timeout= 24*60;

    public function __construct(PropertyRepository $propertyRepository, AgentsRepository $agentsRepository, AdvsRepository $advsRepository, ApplyPropertyRepository $applypropertyRepo)
    {
        if(! isset($_SESSION)){
            session_start();
        }
        $this->sessionToClearOnPageRefresh= array('totalProductPrice');
        $this->errors= array();
        $this->propertyRepository= $propertyRepository;
        $this->agentsRepository= $agentsRepository;
        $this->advsRepository= $advsRepository;
        $this->applypropertyRepo =  $applypropertyRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    // public function productList($page, Request $request, $category_id=null)
    // {   
    //     $data= $this->getPageDetails($page, $request, $category_id);
    //     $items= $data['items']; 
    //     // $maxPrice= $data['maxPrice']; 
    //     Session::put('active_menu','list');
    //     return view('frontend.product.product-list',compact('items'));
    // }

    public function itemsGrid($page, Request $request, $category_id=null)
    {    
        $data= $this->getPageDetails($page, $request, $category_id);
        $items= $data['items']; 
        $hotProperties= $data['hotProperties'];

        Session::put('active_menu','grid');
        return view('frontend.property.property-grid',compact('items','hotProperties'));
    }
    public function getPageDetails($page, $request, $category_id){
        //if the page is changed set value of session variable price_range to 100
        if(isset($_SESSION['page'])){
            if($_SESSION['page']!=$page){
                $_SESSION['price_range']="0-100"; 
            }
        }
        $_SESSION['page']= $page;
        if($page=='category'){   
            $data = $this->getCategoryProduct($category_id,$request); 
            if($this->checkCategoryIsParentOrNot($category_id)){ 
                $data = $this->productOfParentCategory($category_id, $request);
            }  
        }  
        elseif($page=="newArrival"){   
            $data = $this->newArrival($request,$category_id); 
        }
        elseif($page=="search"){   
            $data = $this->search($request,$category_id); 
        }  
        elseif($page=="featured"){   
            $data = $this->featured($request,$category_id); 
        }  
        elseif($page=="relateditems"){  
            $data= $this->getRelateditems($request,$category_id); 
        }
        elseif($page=="appliedProperty"){  
            $data= $this->appliedProperty($request,$category_id); 
        }
        elseif($page=="postedProperty"){  
            $data= $this->postedProperty($request,$category_id); 
        }
        elseif($page=="hotProperties"){  
            $data= $this->hotProperties($request,$category_id); 
        }
        else{
            abort(404);
        } 
        //$return_data['maxPrice']= $data['maxPrice'];
        $items= $data['items'];   
        $return_data['items']= $items->paginate(12); 
        $hotProperties= ($page=="postedProperty") ? $data : $this->hotProperties($request,$category_id);
        $return_data['hotProperties']= $hotProperties['items']->orderBy('view_count','DESC')->get()->take(6);
         
        return $return_data;
    }
     //for filter in side nav, if the value in filter in side nav is
    //changed this function is used in evert method of getting the details of items
    //they are done after this method
    public function filterComponents($items, $request, $category_id=null){  
             
            //if category id is given, show items of that category
            if($category_id != null){
            $items = $items->where('category.id',$category_id);
            }
            //for price range
            if(isset($request['price_range'])){ 
                $_SESSION['price_ranges']=  $request['price_range'];
            }   
            //price range is in number between 0-100 
            //use it as percentage of the product price
            // $price_range= isset($_SESSION['price_ranges']) ? $_SESSION['price_ranges'] : '0-100';
            // $price_rangeArray= explode("-",$price_range); 

            // $minRangePrice= ($price_rangeArray[0]/100) * $maxPrice;
            // $maxRangePrice= ($price_rangeArray[1]/100) * $maxPrice; 
            // $items = $items->whereBetween('property.price', array(round($minRangePrice), round($maxRangePrice)));
            //for sorting items
            $sort= $request['sort'];
            if($sort=='price_desc'){ 
                $items = $items->orderBy('property.price','DESC');
            }
            elseif($sort=='price_asc'){ 
                $items = $items->orderBy('property.price','ASC');
            }
            elseif($sort=='new'){
                $items = $items->orderBy('property.created_at','ASC');
            }
            elseif($sort=='name_desc'){
                $items = $items->orderBy('property.name','DESC');
            }
            elseif($sort=='name_asc'){
                $items = $items->orderBy('property.name','ASC');
            }
            else{
                $items = $items->orderBy('property.name','ASC');
            }

        return $items;

    }
    private function checkCategoryIsParentOrNot($category_id){
        $subCategories= getChildCategoryById($category_id); 
        $is_parent= ($subCategories->count() > 0) ? true : false; 
        return $is_parent;
    } 
  
    //get the default details of the product and show it in product details page
    public function itemDetails($item_id){  
        $itemDetails= DB::table("property")
                    ->join("category","category.id","property.category_id") 
                    ->join("locations","locations.id","property.location_id") 
                    ->join("property_images","property_images.property_id","property.id")
                    ->where('property.deleted_at',null) 
                    ->where('property.active',true) 
                    ->where('property.id',$item_id) 
                    ->select("category.name as category_name","property_images.*","locations.name as location_name","property.*")
                    ->first(); 
        $hotProperties= $this->queryHotProperties()->orderBy('view_count','DESC')->get()->take(6);;
        $agents= $this->agentsRepository->getPaginate(3,['user']);
        $advs= $this->advsRepository->getAll();
        $applied_property_ids= (Auth::check()) ? $this->applypropertyRepo->getAllAppliedPropertyIds(Auth::user()->id) : array();

        return view("frontend.property-detail.property-detail",compact('itemDetails','hotProperties','agents','advs','applied_property_ids'));
    }

     
    //sql queries  
    public function querySearch($request){  
        $search= isset($request['search']) ? $request['search'] : null;  
        $_SESSION['front_property_search']= $search;  
        $category_id=isset($request['category_id']) ? $request['category_id'] : null;  
        $_SESSION['front_property_category_id']= $category_id;  
        $location= isset($request['location']) ? $request['location'] : null;  
        $_SESSION['front_property_location']= $location;  
        $price_range=isset($request['price_range']) ? $request['price_range'] : null;  
        $_SESSION['front_property_price_range']= $price_range;  
        $priceArray= explode("-",$price_range); 
        $start_sale_price= isset($priceArray[0]) ? intval($priceArray[0]) : null;
        $end_sale_price= isset($priceArray[1]) ? intval($priceArray[1]) : null;

        $data = DB::table("property")
                    ->join("category","category.id","property.category_id") 
                    ->join("locations","locations.id","property.location_id") 
                    ->join("property_images","property_images.property_id","property.id")
                    ->where('property.deleted_at',null) 
                    ->where('property.active',true) 
                    ->select("category.*","property_images.*","locations.*","property.*");
        if($location){ 
            $data=$data->where('locations.name','like',"%$location%");
        }
        if($location){ 
            $data=$data->where('locations.name','like',"%$location%");
        }
        if($start_sale_price){
            $data= $data->where('property.price','>=',$start_sale_price)   
                          ->where('property.price','<=',$end_sale_price);
        } 
        if($search){ 
            $data= $data->where('property.name','like',"%$search%")
            ->orWhere('property.sku','like',"%$search%") 
            ->orWhere('property.keywords','like',"%$search%") ;  
        }
        return $data; 
    }
    public function queryCategoryProduct($category_id){
        $items = DB::table("property")
                    ->join("category","category.id","property.category_id")  
                    ->join("property_images","property_images.property_id","property.id") 
                    ->where('property.deleted_at',null)
                    ->where('property.active', true) 
                    ->where('category.id',$category_id)
                    ->select("category.*","property_images.*","property.*");
        return $items;
    }
    public function queryFeatured($category_id=null){
        $items = DB::table("featured")
                    ->join("property","property.id","featured.property_id")
                    ->join("category","category.id","property.category_id") 
                    ->join("property_images","property_images.property_id","property.id")  
                    ->where('property.deleted_at',null)
                    ->where('property.active', true)
                    ->select("featured.*","category.*","property_images.*","property.*");

        return $items;
    }
    public function queryOnSale(){
        $nowString= Carbon::now(getCurrentTimeZone())->toDateTimeString();
        $items = DB::table("featured")
                    ->join("property","property.id","featured.property_id")
                    ->join("category","category.id","property.category_id") 
                    ->join("property_images","property_images.property_id","property.id") 
                    ->where('property.deleted_at',null)
                    ->where('property.active', true) 
                    ->where('property.sale_price','>',0) 
                    ->whereRaw('property.sale_price < items.price')
                    ->whereDate('property.sale_from','<=',$nowString)
                    ->whereDate('property.sale_to','>=',$nowString) 
                    ->select("featured.*","category.*","property_images.*","property.*"); 

        return $items;
    }
     
    public function queryRelated($category_id=null){
        $items = DB::table("featured")
                    ->join("property","property.id","featured.property_id")
                    ->join("category","category.id","property.category_id")  
                    ->join("property_images","property_images.property_id","property.id")
                    ->where('property.deleted_at',null)
                    ->where('property.active', true)  
                    ->select("featured.*","category.*","property_images.*","property.*");
        return $items;
    }
    public function queryAppliedProperty(){
        $items = DB::table("property")
                    ->join("users","users.id","property.user_id")
                    ->join("apply_property","apply_property.user_id","users.id")
                    ->join("category","category.id","property.category_id")  
                    ->join("property_images","property_images.property_id","property.id")
                    ->where('property.deleted_at',null)
                    ->where('property.active', true) 
                    ->where('property.user_id',Auth::user()->id)
                    ->select("apply_property.*","users.*","category.*","property_images.*","property.*");
        return $items;
    }
    public function queryPostedProperty(){
        $items = DB::table("property")
                    ->join("users","users.id","property.user_id")
                    ->join("category","category.id","property.category_id")
                    ->join("property_images","property_images.property_id","property.id")  
                    ->where('property.deleted_at',null)
                    ->where('property.active', true)
                    ->where('property.user_id',Auth::user()->id)
                    ->select("users.*","category.*","property_images.*","property.*");
        return $items;
    }

    public function queryHotProperties(){
        $items = DB::table("property") 
                    ->join("property_images","property_images.property_id","property.id")  
                    ->where('property.deleted_at',null)
                    ->where('property.active', true) 
                    ->select("property_images.*","property.*");
        return $items;
    }
    
   
    //this method changes the session value of the sortings
    public function toggleSort($sort){ 
        if($sort=="price_desc"){
            $_SESSION['sortPrice']= 'price_asc';
        } 
        if($sort=="price_asc"){
            $_SESSION['sortPrice']= 'price_desc';
        }
        if($sort=="name_desc"){
            $_SESSION['sortName']= 'name_asc';
        }
        if($sort=="name_asc"){
            $_SESSION['sortName']= 'name_desc';
        }
        
        return true;
    }
    //this method changes the session value for recommendation
    public function setSearchForRecommendation($search){
        $_SESSION['search_recommendation']= array();
        $_SESSION['search_recommendation'][]= $search;
        return false;
    }

    //for displaying items according to search
    public function search($request, $category_id=null){  
        //$this->setSearchForRecommendation($search);

        // $maxPrice= $this->getMaximumPriceSearch($search);
        $items= $this->querySearch($request); 
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){ 
            $this->toggleSort($request['sort']); 
        } 
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }
    //for displaying new arrival items
    public function featured($request, $category_id=null){ 
        //$maxPrice= $this->getMaximumPriceNewArrival(); 
        $items= $this->queryFeatured();
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){
            $this->toggleSort($request['sort']);
        }
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }
    
    //get products of certain category
    public function getCategoryProduct($category_id, $request=null){
        //$maxPrice= $this->getMaximumPriceProduct($category_id); 
        $products= $this->queryCategoryProduct($category_id);
        $products= $this->filterComponents($products, $request, $category_id); 
         if($products){
            $this->toggleSort($request['sort']);
        } 
        $data['products']=$products;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }
    //for displaying new arrival items
    public function hotProperties($request, $category_id=null){ 
        //$maxPrice= $this->getMaximumPriceNewArrival(); 
        $items= $this->queryHotProperties();
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){
            $this->toggleSort($request['sort']);
        }
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
         
    }
    //for displaying new arrival items
    public function onSale($request, $category_id=null){ 
        //$maxPrice= $this->getMaximumPriceNewArrival(); 
        $items= $this->queryOnSale();
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){
            $this->toggleSort($request['sort']);
        }
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }
    //for displaying new arrival items
    public function related($request, $category_id=null){ 
        //$maxPrice= $this->getMaximumPriceNewArrival(); 
        $items= $this->queryRelated();
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){
            $this->toggleSort($request['sort']);
        }
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }
    public function postedProperty($request, $category_id=null){ 
        //$maxPrice= $this->getMaximumPriceNewArrival(); 
        $items= $this->queryPostedProperty();
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){
            $this->toggleSort($request['sort']);
        }
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }
    public function appliedProperty($request, $category_id=null){ 
        //$maxPrice= $this->getMaximumPriceNewArrival(); 
        $items= $this->queryAppliedProperty();
        $items= $this->filterComponents($items, $request, $category_id); 
        if($items){
            $this->toggleSort($request['sort']);
        }
        $data['items']=$items;
        //$data['maxPrice']=$maxPrice;
        return $data;
    }




     
    

}
