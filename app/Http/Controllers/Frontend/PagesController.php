<?php

namespace App\Http\Controllers\Frontend;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         
    }

    public function about(){
        return view('frontend.pages.about-us');
    }

    public function privacy(){
        return view('frontend.pages.privacy');
    }

    public function deliveryInfo(){
        return view('frontend.pages.delivery-info');
    }

    public function termCondition(){
        return view('frontend.pages.terms-condition');
    }
    public function contactUs(){
        return view('frontend.pages.contact-us');
    }
    public function whyShop(){
        return view('frontend.pages.why-shop');
    }
    public function howToShop(){
        return view('frontend.pages.how-to-shop');
    }

    
}
