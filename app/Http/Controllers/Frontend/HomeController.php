<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller; 
use Modules\Featured\FeaturedRepository; 
use Modules\Property\PropertyRepository;
use Modules\Sliders\SlidersRepository; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FeaturedRepository $featuredRepository,PropertyRepository $propertyRepository,SlidersRepository $slidersRepository)
    { 
        $this->featuredRepository = $featuredRepository;
        $this->propertyRepository = $propertyRepository;
        $this->slidersRepository = $slidersRepository;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
         
        $featured= $this->featuredRepository->getPaginate(8,['property','property_images']);
        $newProperties= $this->propertyRepository->getOrderBy(8,['id'=>'DESC'],['property_images']);
        $sliders= $this->slidersRepository->getAllOrderBy(['order'=>'ASC']); 
        $recommended= array();  
        return view('frontend.home.index',compact('featured','recommended','newProperties','sliders'));
    }
    
}
