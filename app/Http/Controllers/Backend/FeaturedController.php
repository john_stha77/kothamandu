<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Featured;
use File;
use Auth;
use DB;  

class FeaturedController extends Controller
{
    protected $page= "Featured";
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(){
        
    }
    public function index()
    {    
         
        $posts= Featured::where('deleted_at', null)->orderBy('id','desc')->paginate(10); 
        return view('backend.featured.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('backend.featured.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
             
        ]);  

        $result= Featured::create($request->all());
 
        return redirect()->route('featured.index')->withFlashSuccess('It has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Featured::findOrFail($id);
       
        return view('backend.featured.edit', compact('posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
             
        ]);
         
        $featured = Featured::findOrFail($id);
        $featured->update($request->all()); 
        
        return redirect()->route('featured.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result= Featured::findOrFail($id);
        $data['deleted_at']= deletedTime();
        $result->update($data);
        return redirect()->route('featured.index')->withFlashSuccess('It is deleted successfully.');
    }
}
