<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Advs;
use File;
use Auth;
use DB;  
use App\Http\Controllers\FileUploadController;

class AdvsController extends Controller
{
    protected $page= "Advs";
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(){
        
    }
    public function index()
    {    
         
        $posts= Advs::where('deleted_at', null)->orderBy('id','desc')->paginate(15); 
        return view('backend.advs.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('backend.advs.create', compact('advs','supplier','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
             
        ]);
        $file= $request->file('icon');
        $filepath= '/files/advs/';
        
        $uploadInstance= new FileUploadController();
        $upload= $uploadInstance->file_upload($file, $filepath, $watermark=true, $resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type'];
             
        }
         
         
 
        $result= Advs::create($request->all());

        
        return redirect()->route('advs.index')->withFlashSuccess('It has been added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Advs::findOrFail($id);
       
        return view('backend.advs.edit', compact('posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
             
        ]);
        $filepath= '/files/advs/';
        if($request->hasFile('icon'))
        {   
            //delete previous icon
            $p_icon = Advs::findOrFail($id);
            $file_deleted= fileDeleteIfExist($filepath,$p_icon->image_file_name);
        }
        $file= $request->file('icon');
        
        $fileupload= new FileUploadController();
        $upload= $fileupload->file_upload($file, $filepath,$watermark=true,$resize=true);
        if($upload)
        {
            $request['image_file_name']=  $upload['file_name'];
            $request['image_file_size']= $upload['file_size'];
            $request['image_content_type'] = $upload['content_type'];
            $request['image_updated_at']= date("F d, Y h:i:s A"); 
        }
 
        $advs = Advs::findOrFail($id);
        $advs->update($request->all()); 
         
        return redirect()->route('advs.edit', $id)->withFlashSuccess('It is updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result= Advs::findOrFail($id);
        $data['deleted_at']= deletedTime();
        $result->update($data);
        return redirect()->route('advs.index')->withFlashSuccess('It is deleted successfully.');
    }
}
