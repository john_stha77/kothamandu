<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\Featured;
use Auth;

class PropertyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Dispropertyy a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts= Property::where('deleted_at',null)->paginate(10);
        $featured= array();
        $p_ids= array();
        foreach ($posts as $p) {
            $p_ids[]= $p->id; 
        }
        $featureds= Featured::whereIn('property_id',$p_ids)->get();
        foreach ($featureds as $f) {
            $featured[]= $f->property_id;
        }
        return view('backend.property.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      
        return view('backend.property.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
 
        ]);
        $request['created_by']= Auth::user()->id;  
        $result= Property::create($request->all());
 
        return redirect()->route('property.index')->withFlashSuccess('Property has been added successfully.');
    }

    /**
     * Dispropertyy the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = Property::findOrFail($id);
       
        return view('backend.property.edit', compact('posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            
        ]);
        $request['updated_by']= Auth::user()->id;  
        $property = Property::findOrFail($id);
        $property->update($request->all());
        return redirect()->route('backend.property.edit', $id)->withFlashSuccess('Property is updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Property::destroy($id);
        return redirect()->route('property.index')->withFlashSuccess('Property is deleted successfully.');
    }
}
