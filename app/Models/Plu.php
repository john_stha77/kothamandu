<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plu extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plu';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    
    protected $guarded = ['id'];

}
