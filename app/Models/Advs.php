<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Advs extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    use SoftDeletes; 

    protected $table = 'advs';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    
    protected $guarded = ['id','icon'];

}
