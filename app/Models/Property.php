<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'property';
    
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    
    protected $guarded = ['id'];

}
