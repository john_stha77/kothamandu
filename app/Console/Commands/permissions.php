<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class Permissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permissions:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all routes and update the permissions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //get all routes
        $app = app();
        $routes = $app->routes->getRoutes(); 
        
        $countUpdated=0; 
        foreach ($routes as $route){
            if($route->getName() !=null){
                $data['code']= $route->getName();
                $data['name']= str_replace(".","-",$data['code']);
                //check if the permission exists
                $permission= DB::table("permissions")->where('code',$data['code'])->first();

                //if permission doesnot exist insert to the database
                //else do nothing 
                if(! $permission){
                    DB::table('permissions')->insert($data);
                    $this->info("Saved permission code: ".$data['code']." name: ".$data['name']); 
                    $countUpdated++;
                } 
                
            } 
        }
        if($countUpdated==0){
             $this->info("No items to update.");
        }
    }
}
