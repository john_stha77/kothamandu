<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class frontend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'frontend:make {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make frontend basic files.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    //function to get the content inside file
    protected function getStub($type)
    {
        return file_get_contents(app_path("Console/Commands/crud/stubs/$type.stub"));
    }
    //get template from the stubs
    protected function getTemplate($stubName,$name){
        $template = str_replace(
            [
                '{{modelName}}',
                '{{modelNamePluralLowerCase}}',
                '{{modelNameSingularLowerCase}}'
            ],
            [
                $name,
                strtolower(str_plural($name)),
                strtolower($name)
            ],
            $this->getStub($stubName)
        );
        return $template;
    }
    protected function makeDirIfAbsent($path){
        $check=false;
        if(!is_dir($path)){ 
            mkdir($path, 0777, true);
            $check=true;
        }
        return $check;
    }

    //add the content of Routes.stub and make Routes file
    protected function routes($name)
    {
        $check=false;
        $routesTemplate = $this->getTemplate('Routes',$name);

        $this->makeDirIfAbsent($this->path.$name);
        $file= $this->path.$name."/routes.php";
        $result=file_put_contents($file, $routesTemplate);
        if($result !=null){
            $check= true;
        }
        //register this route to routesServiceProvider in modules folder
        $filename = base_path("Modules/RoutesServiceProvider.php"); // the file to change
        $search = "public  function runRoutes(){"; // the content after which you want to insert new stuff
        $insert = "\t\trequire_once(base_path('Modules/$name/routes.php'));"; // your new stuff 
        $replace = $search. "\n". $insert; 
        file_put_contents($filename, str_replace($search, $replace, file_get_contents($filename)));

        return $check;
    }
    //add the content of Controller.stub and make Controller file
    protected function controller($name)
    {
        $check=false;
        $controllerTemplate = $this->getTemplate('Controller',$name);
        $this->makeDirIfAbsent($this->path.$name);
        $file= $this->path.$name."/".$name."Controller.php";
        $result=file_put_contents($file, $controllerTemplate);
        if($result !=null){
            $check= true;
        }
        return $check; 
    }
    //add the content of Routes.stub and make Routes file
    protected function views($name, $options=null)
    {
        $check=false;
        $template['index'] = $this->getTemplate('ViewIndex',$name);
        $template['create'] = $this->getTemplate('ViewCreate',$name);
        $template['edit'] = $this->getTemplate('ViewEdit',$name);

        $this->makeDirIfAbsent($this->path.$name."/views/");
        foreach($template as $key=>$val){
            $file= $this->path.$name."/views/".$key.".blade.php";
            $result=file_put_contents($file, $val);
            if($result !=null){
                $check= true;
            } 
        }
        //make form.blade.php file
        $this->makeDirIfAbsent($this->path.$name."/views/form/");
        $file= $this->path.$name."/views/form/form.blade.php";
        $result=file_put_contents($file, "");

        //write link list to sidebar
        $filename = resource_path("views/layouts/backend/sidebar.blade.php"); // the file to change
        $search = "<ul class='nav side-menu'>"; // the content after which you want to insert new stuff
        $routeName= "{{route('".(strtolower($name)).".index')}}";
        $tabSpace= "\t\t\t\t\t\t\t\t\t";
        $insert = $tabSpace.'<li><a  href='.$routeName.'><i class="fa fa-briefcase"></i>'.$name.'</a></li>'; // your new stuff 
        $replace = $search. "\n". $insert; 
        file_put_contents($filename, str_replace($search, $replace, file_get_contents($filename)));

        return $check;
    }
    public function handle()
    {
        $this->info("Basic frontend files are made successfully."); 
    }
}
