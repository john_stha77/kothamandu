<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use DB;
use Cache;
use Illuminate\Support\Facades\Route;
use Modules\Category\CategoryModel;  

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    protected $cache_timeout= 12*60;
     
    function getGenerals(){
        $generalsInstance= DB::table('generals')->first();
        $generals['logo_file_name']= isset($generalsInstance->logo_file_name) ? $generalsInstance->logo_file_name : null;
        $generals['title_dashboard']= isset($generalsInstance->title_dashboard) ? $generalsInstance->title_dashboard : null;
        $generals['title_frontpage']=  isset($generalsInstance->title_frontpage) ? $generalsInstance->title_frontpage : null; 
        $generals['favicon_file_name']=  isset($generalsInstance->favicon_file_name) ? $generalsInstance->favicon_file_name : null; 
        $generals['default_image_name']=  isset($generalsInstance->default_image_name) ? $generalsInstance->default_image_name : null; 
        $generals['meta_description_front']=  isset($generalsInstance->meta_description_front) ? $generalsInstance->meta_description_front : null;

        return $generals;
    }

    public function boot()
    {
        View::composer('*', function ($view) {  
            
            $generals=  Cache::remember('getGenerals',$this->cache_timeout+1, function(){
                return $this->getGenerals();
            });    
            $view->with('generals',$generals); 
            //return current route name
            $currentRouteName= Route::currentRouteName();
            $view->with('currentRouteName',$currentRouteName);  

            $categories=  Cache::remember('categories',$this->cache_timeout+1, function(){
                return CategoryModel::where("active",true)->get();
            });
            $view->with('categories',$categories);
             
        }); 
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
